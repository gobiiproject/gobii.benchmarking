/* fetchmarkerlist_rand_mpi.c, 24apr20, from: */
/* fetchmarkerlist_rand.c, 7may17, from fetchmarkerlist_test.c, 26apr17, from fetchmarkerlist.c, 8jun16...
   Fetch a set of entire rows, alleles for all samples for the specified list of markers. */
/* 13jun16, Detect and handle different datatypes. */
/* 7dec16, Return a row of "N"s if marker number is -1. */
/* 7dec16, Take the list of marker positions from a file instead of command arguments. */
/* 26apr17, Add parameter to query only the first n markers from the list. */
/* 7may17, Generate the list of marker positions internally with rand(). */
/* 24apr20, Parallelize with MPI-CH. */

#include "hdf5.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <mpi.h>
#include <mpio.h>

#define DATASETNAME "/allelematrix" 
#define RANK  2                           /* number of dimensions */

H5_DLL herr_t H5Pset_fapl_mpio(hid_t fapl_id, MPI_Comm comm, MPI_Info info);
H5_DLL herr_t H5Pset_dxpl_mpio(hid_t dxpl_id, H5FD_mpio_xfer_t xfer_mode);

int main (int argc, char *argv[]) {

  /* HDF5 variables */
  hsize_t     filedims[RANK], dimsm[RANK];   
  hsize_t     start[RANK], stride[RANK], count[RANK], block[RANK];
  hid_t       file_id, dataset_id;             /* handles */
  hid_t       dataspace_id, memspace_id; 
  hid_t       datumtype;
  hid_t       faplist_id, dxplist_id;          /* property list handles */
  herr_t      status;                   

  FILE *outfile;
  int datumsize, i, j, k;

  if (argc < 3) {
    printf("Usage: mpiexec -np <processors> %s <HDF5 file> <markercount>\n", argv[0]);
    printf("E.g. mpiexec -np 4 %s ../data/generated_dataset_snp.h5 1000\n", argv[0]);
    printf("Fetch alleles for <markercount> random marker positions, for all samples.\n");
    return 0;
  }
  /* Read the arguments. */
  char *h5filename = argv[1];
  int markercount = atoi(argv[2]);
  char *outfilename = argv[3];
  outfile = fopen (outfilename, "w");

  time_t starttime = time(NULL);

  /* MPI variables     */
  int mpi_size, mpi_rank;  /* total number or processors; number of this processor */
  MPI_Comm comm  = MPI_COMM_WORLD;
  MPI_Info info  = MPI_INFO_NULL;

  /* Initialize MPI     */
  MPI_Init(&argc, &argv);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);  
 
  /* Write a log of the results. */
  FILE *logfile = fopen ("markerstest.log", "a");
  if (mpi_rank == 0) {
    fprintf(logfile, "%s\n", argv[0]);
    fprintf(logfile, "Number of markers: %i\n", markercount);
    fprintf(logfile, "Mode: random\n");
  }

  /* Set up file access property list with parallel I/O access     */
  faplist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(faplist_id, comm, info);

  /* Open the HDF5 file and dataset. */
  file_id = H5Fopen (h5filename, H5F_ACC_RDONLY, faplist_id);
  dataset_id = H5Dopen2 (file_id, DATASETNAME, H5P_DEFAULT);
  dataspace_id = H5Dget_space (dataset_id);

  /* Find the dimensions of the HDF5 file dataset. */
  H5Sget_simple_extent_dims(dataspace_id, filedims, NULL);
  int MarkerTotal = filedims[0];
  int SampleTotal = filedims[1];

  /* Determine the datatype and the size of an individual element. */
  datumtype = H5Dget_type(dataset_id);
  datumsize = H5Tget_size(datumtype);

  /* Create memory space with size of subset. */
  dimsm[0] = 1;
  dimsm[1] = SampleTotal; 
  memspace_id = H5Screate_simple (RANK, dimsm, NULL); 
  char *rdata = malloc(SampleTotal * datumsize);  /* buffer for read */
  /* Select subset from file dataspace. */
  start[1] = 0;
  stride[0] = 1; stride[1] = 1;
  count[0] = 1; count[1] = 1;
  block[0] = 1; block[1] = SampleTotal;

  /* Create property list for collective dataset read. */
  dxplist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(dxplist_id, H5FD_MPIO_COLLECTIVE);

  /* Each process picks some random marker positions and outputs the matrix rows for them. */
  char tempfilename[] = "tempxx";
  tempfilename[4] = (char)(mpi_rank+65); // process 0 = "A", then B, C, ... 
  tempfilename[5] = '\0';
  FILE *tempfile = fopen(tempfilename, "w");
  int marker;
  srand(starttime);
  for (i = 0; i < markercount/mpi_size; ++i) {
    marker = rand() % 20000000;
    if (marker >= MarkerTotal) {
      printf("Marker number %i out of range.\n", marker);
      return 1;
    }
    start[0] = marker;
    status = H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, start, stride, count, block);
    /* Read the hyperslab. */
    status = H5Dread (dataset_id, datumtype, memspace_id, dataspace_id, dxplist_id, rdata);
    /* Write the results to this process's output file, as a tab-delimited string for each marker. */
    for (j = 0; j < SampleTotal * datumsize; j = j + datumsize) {
      for (k = 0; k < datumsize; k++) 
	fprintf(tempfile, "%c", rdata[j + k]);
      /* No trailing <Tab> at end of line. */
      if (j < (SampleTotal - 1) * datumsize)
	fprintf(tempfile, "\t");
    }
    fprintf(tempfile, "\n");
  }
  fclose(tempfile);

  /* Make sure all processes have done writing. */
  MPI_Barrier(MPI_COMM_WORLD);

  /* Catenate the temp files. */
  if (mpi_rank  == 0) {
    status = system("cat temp* > fetchmarkerlist.out");
    status = system("rm temp*");
  }

  time_t stoptime = time(NULL);
  int elapsed = stoptime - starttime;
  if (mpi_rank == 0) {
    fprintf(logfile, "Elapsed time: %i seconds, %i processors\n\n", elapsed, mpi_size);
    fclose(logfile);
  }
  status = H5Tclose(datumtype);
  status = H5Sclose (memspace_id);
  status = H5Sclose (dataspace_id);
  status = H5Dclose (dataset_id);
  status = H5Fclose (file_id);
  if (status >= 0) return 0;
  else return 1;

  MPI_Finalize();
}
