###### 1M markers x ALL 3k samples #########
#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id <= 100100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_1.csv' with CSV DELIMITER E'\t';
" &
#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 100100 and marker_genotype_id <= 200100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_2.csv' with CSV DELIMITER E'\t';
" &
#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 200100 and marker_genotype_id <= 300100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_3.csv' with CSV DELIMITER E'\t';
" &
#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 300100 and marker_genotype_id <= 400100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_4.csv' with CSV DELIMITER E'\t';
" &
#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 400100 and marker_genotype_id <= 500100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_5.csv' with CSV DELIMITER E'\t';
" &
#6
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 500100 and marker_genotype_id <= 600100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_6.csv' with CSV DELIMITER E'\t';
" &
#7
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 600100 and marker_genotype_id <= 700100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_7.csv' with CSV DELIMITER E'\t';
" &
#8
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 700100 and marker_genotype_id <= 800100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_8.csv' with CSV DELIMITER E'\t';
" &
#9
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 800100 and marker_genotype_id <= 900100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_9.csv' with CSV DELIMITER E'\t';
" &
#10
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id > 900100 and marker_genotype_id <= 1000100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_10.csv' with CSV DELIMITER E'\t';
" &
