
#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id <= 100000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_1.csv' with CSV DELIMITER E'\t';
" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 100000 and mi.id <= 200000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_2.csv' with CSV DELIMITER E'\t';
" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 200000 and mi.id <= 300000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_3.csv' with CSV DELIMITER E'\t';
" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 300000 and mi.id <= 400000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_4.csv' with CSV DELIMITER E'\t';
" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 400000 and mi.id <= 500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_5.csv' with CSV DELIMITER E'\t';
" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 500000 and mi.id <= 600000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_6.csv' with CSV DELIMITER E'\t';
" &

#6
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 600000 and mi.id <= 700000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_7.csv' with CSV DELIMITER E'\t';
" &

#8
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 700000 and mi.id <= 800000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_8.csv' with CSV DELIMITER E'\t';
" &

#9
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 800000 and mi.id <= 900000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_9.csv' with CSV DELIMITER E'\t';
" &

#10
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1m mi on mgs.marker_name=mi.marker_id
where mi.id > 900000 and mi.id <= 1000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples_nonctg_10.csv' with CSV DELIMITER E'\t';
" &
