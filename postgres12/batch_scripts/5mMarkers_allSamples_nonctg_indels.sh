###### 5M markers x ALL samples non-contiguous #########
#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id <= 500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_1.csv' with CSV DELIMITER E'\t';
" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 500000 and mi.id <= 1000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_2.csv' with CSV DELIMITER E'\t';
" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 1000000 and mi.id <= 1500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_3.csv' with CSV DELIMITER E'\t';
" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 1500000 and mi.id <= 2000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_4.csv' with CSV DELIMITER E'\t';
" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 2000000 and mi.id <= 2500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_5.csv' with CSV DELIMITER E'\t';
" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 2500000 and mi.id <= 3000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_6.csv' with CSV DELIMITER E'\t';
" &

#6
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 3000000 and mi.id <= 3500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_7.csv' with CSV DELIMITER E'\t';
" &

#8
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 3500000 and mi.id <= 4000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_8.csv' with CSV DELIMITER E'\t';
" &

#9
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 4000000 and mi.id <= 4500000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_9.csv' with CSV DELIMITER E'\t';
" &

#10
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_5m mi on mgs.marker_name=mi.marker_id
where mi.id > 4500000 and mi.id <= 5000000) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples_nonctg_10.csv' with CSV DELIMITER E'\t';
" &
