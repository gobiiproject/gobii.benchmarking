#!/bin/sh
###### All markers x 100 samples contiguous #########

# One partition:
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
	     from marker_genotype_snp mgs)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_A.csv' with CSV DELIMITER E'\t';" &

# #1
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id <= 4000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_A.csv' with CSV DELIMITER E'\t';" &

# #2
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_B.csv' with CSV DELIMITER E'\t';" &

# #3
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_C.csv' with CSV DELIMITER E'\t';" &

# #
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_D.csv' with CSV DELIMITER E'\t';" &

# #5
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_E.csv' with CSV DELIMITER E'\t';" &

# #6
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 10000000 and mgs.marker_genotype_id <= 12000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_F.csv' with CSV DELIMITER E'\t';" &

# #7
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 14000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_G.csv' with CSV DELIMITER E'\t';" &

# #8
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 14000000 and mgs.marker_genotype_id <= 16000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_H.csv' with CSV DELIMITER E'\t';" &

# #9
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 18000000)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_I.csv' with CSV DELIMITER E'\t';" &

# #10
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
# 	     from marker_genotype_snp mgs
#              where mgs.marker_genotype_id > 18000000 and mgs.marker_genotype_id <= 20000100)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_J.csv' with CSV DELIMITER E'\t';" &

# Ten partitions:
# Time: 946090.321 ms (15:46.090)
# Time: 949466.687 ms (15:49.467)
# Time: 950038.845 ms (15:50.039)
# Time: 950334.709 ms (15:50.335)
# Time: 950785.473 ms (15:50.785)
# Time: 1300148.284 ms (21:40.148)
# Time: 2562476.343 ms (42:42.476)
# Time: 2574300.717 ms (42:54.301)
# Time: 2580762.648 ms (43:00.763)
# Time: 2594932.413 ms (43:14.932)
#
# Time: 945253.100 ms (15:45.253)
# Time: 946484.384 ms (15:46.484)
# Time: 948150.388 ms (15:48.150)
# Time: 949055.759 ms (15:49.056)
# Time: 950598.509 ms (15:50.599)
# Time: 1296896.068 ms (21:36.896)
# Time: 2569275.058 ms (42:49.275)
# Time: 2574319.636 ms (42:54.320)
# Time: 2589230.952 ms (43:09.231)
# Time: 2589553.463 ms (43:09.553)
#
# Five partitions:
# Time: 1515971.251 ms (25:15.971)
# Time: 1525837.104 ms (25:25.837)
# Time: 1526619.823 ms (25:26.620)
# Time: 1527610.768 ms (25:27.611)
# Time: 1528677.534 ms (25:28.678)
#
# Time: 1525663.337 ms (25:25.663)
# Time: 1528433.876 ms (25:28.434)
# Time: 1528824.177 ms (25:28.824)
# Time: 1530579.576 ms (25:30.580)
# Time: 1530628.518 ms (25:30.629)
#
# One partition:
# Time: 3396897.584 ms (56:36.898)
# Time: 3416123.513 ms (56:56.124)
