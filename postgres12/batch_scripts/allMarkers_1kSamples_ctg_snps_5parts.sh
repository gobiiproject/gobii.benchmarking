#!/bin/sh
###### All markers x 1000 samples contiguous #########

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

# # 5 partitions:
# Time: 2215830.246 ms (36:55.830)
# Time: 2216342.977 ms (36:56.343)
# Time: 2216705.961 ms (36:56.706)
# Time: 2223152.981 ms (37:03.153)
# Time: 2224362.463 ms (37:04.362)
#
# Time: 2211679.352 ms (36:51.679)
# Time: 2221230.684 ms (37:01.231)
# Time: 2223856.118 ms (37:03.856)
# Time: 2224377.156 ms (37:04.377)
# Time: 2228507.054 ms (37:08.507)
