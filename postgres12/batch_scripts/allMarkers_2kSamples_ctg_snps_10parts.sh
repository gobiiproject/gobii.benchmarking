#!/bin/sh
###### All markers x 2000 samples contiguous #########

# One partition:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
# 	     from marker_genotype_snp mgs)
# to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id <= 2000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 2000000 and mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 6000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 6000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 10000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

#6
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 10000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_F.csv' with CSV DELIMITER E'\t';" &

#7
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 14000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_G.csv' with CSV DELIMITER E'\t';" &

#8
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 14000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_H.csv' with CSV DELIMITER E'\t';" &

#9
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 18000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_I.csv' with CSV DELIMITER E'\t';" &

#10
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 2000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 18000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg_J.csv' with CSV DELIMITER E'\t';" &

# 1 partition:
# Time: 6635403.536 ms (01:50:35.404)

# 5 partitions:
# Time: 2981835.206 ms (49:41.835)
# Time: 2982387.164 ms (49:42.387)
# Time: 2992030.695 ms (49:52.031)
# Time: 2992113.423 ms (49:52.113)
# Time: 2994377.960 ms (49:54.378)
#
# Time: 2986006.806 ms (49:46.007)
# Time: 2988673.179 ms (49:48.673)
# Time: 2990480.866 ms (49:50.481)
# Time: 2992008.493 ms (49:52.008)
# Time: 2992083.912 ms (49:52.084)

# 10 partitions:
# Time: 1841321.099 ms (30:41.321)
# Time: 1844756.285 ms (30:44.756)
# Time: 1846476.964 ms (30:46.477)
# Time: 1851559.743 ms (30:51.560)
# Time: 1855830.356 ms (30:55.830)
# Time: 2528814.325 ms (42:08.814)
# Time: 4954678.514 ms (01:22:34.679)
# Time: 5001145.710 ms (01:23:21.146)
# Time: 5036972.931 ms (01:23:56.973)
# Time: 5065535.019 ms (01:24:25.535)
