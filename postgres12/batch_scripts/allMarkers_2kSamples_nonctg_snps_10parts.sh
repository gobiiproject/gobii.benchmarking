#!/bin/sh
###### All markers x 2000 samples non-contiguous #########
#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id <= 2000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 2000000 and mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 6000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 6000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 10000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_E.csv' with CSV DELIMITER E'\t';" &

#6
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 10000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_F.csv' with CSV DELIMITER E'\t';" &

#7
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 14000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_G.csv' with CSV DELIMITER E'\t';" &

#8
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 14000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_H.csv' with CSV DELIMITER E'\t';" &

#9
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 18000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_I.csv' with CSV DELIMITER E'\t';" &

#10
# Note: Numbering of marker_genotype_id begins at 100.  Lets get them all!
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 18000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_J.csv' with CSV DELIMITER E'\t';" &

# Time: 2719593.551 ms (45:19.594)
# Time: 2726633.564 ms (45:26.634)
# Time: 2731553.492 ms (45:31.553)
# Time: 2731863.347 ms (45:31.863)
# Time: 2740633.331 ms (45:40.633)
# Time: 3811581.166 ms (01:03:31.581)
# Time: 7565450.763 ms (02:06:05.451)
# Time: 7572988.405 ms (02:06:12.988)
# Time: 7631524.644 ms (02:07:11.525)
# Time: 7696127.016 ms (02:08:16.127)
# $ time cat allMarkers_2kSamples_*.csv > allMarkers_2kSamples.csv
# real   5m23.347s
# 32 cpus, all at 100%

