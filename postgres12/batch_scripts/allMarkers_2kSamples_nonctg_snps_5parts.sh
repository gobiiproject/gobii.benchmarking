#!/bin/sh
###### All markers x 2000 samples non-contiguous #########
# Divide the table into five partitions.

# -- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
# -- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
# truncate samples_input;
# copy public.samples_input (sample_id)
# FROM PROGRAM 'cat /data/gobii.benchmarking/randomlists/SNPs_only/pg_samples.2000 | tr "\n" "\t"'
# DELIMITER E',';

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# explain analyze
# select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
# from marker_genotype_snp mgs, samples_input si
# where mgs.marker_genotype_id <= 4000000
# order by marker_genotype_id;"

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples_E.csv' with CSV DELIMITER E'\t';" &

# Time: 4425010.431 ms (01:13:45.010)
# Time: 4430044.754 ms (01:13:50.045)
# Time: 4438631.771 ms (01:13:58.632)
# Time: 4440069.963 ms (01:14:00.070)
# Time: 4448418.068 ms (01:14:08.418)
# 32 cpus at >= 90%
#
# Time: 4426014.555 ms (01:13:46.015)
# Time: 4434389.766 ms (01:13:54.390)
# Time: 4436847.885 ms (01:13:56.848)
# Time: 4444369.215 ms (01:14:04.369)
# Time: 4445200.453 ms (01:14:05.200)
#
# With OrderedDict turned off:
# Time: 3329302.770 ms (55:29.303)
# Time: 3330070.432 ms (55:30.070)
# Time: 3332006.381 ms (55:32.006)
# Time: 3335770.087 ms (55:35.770)
# Time: 3338765.689 ms (55:38.766)
#
# With json.loads turned off:  (Replicate #1)
# Time: 2420835.811 ms (40:20.836)
# Time: 2421258.957 ms (40:21.259)
# Time: 2424418.087 ms (40:24.418)
# Time: 2427309.576 ms (40:27.310)
# Time: 2427741.771 ms (40:27.742)
#
# As above but with "order by marker_genotype_id":
# Time: 7894582.666 ms (02:11:34.583)
# Time: 7922934.414 ms (02:12:02.934)
# Time: 7932703.790 ms (02:12:12.704)
# Time: 7933873.347 ms (02:12:13.873)
# Time: 7942596.734 ms (02:12:22.597)
#
# With json.loads and "order by" turned off:  (Replicate #2)
# Time: 2417186.649 ms (40:17.187)
# Time: 2424897.928 ms (40:24.898)
# Time: 2427166.728 ms (40:27.167)
# Time: 2430444.064 ms (40:30.444)
# Time: 2433301.389 ms (40:33.301)
