#!/bin/sh
###### All markers x 3000 samples contiguous #########

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

# 5 partitions:
# Time: 3745760.079 ms (01:02:25.760)
# Time: 3750821.506 ms (01:02:30.822)
# Time: 3751985.029 ms (01:02:31.985)
# Time: 3754290.923 ms (01:02:34.291)
# Time: 3757230.727 ms (01:02:37.231)
#
# Time: 3753070.806 ms (01:02:33.071)
# Time: 3753537.234 ms (01:02:33.537)
# Time: 3753601.509 ms (01:02:33.602)
# Time: 3759842.215 ms (01:02:39.842)
# Time: 3765146.517 ms (01:02:45.147)
