#!/bin/sh
###### All markers x 500 samples contiguous #########

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 500, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 500, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 500, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 500, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 500, 'variant.')
	     from marker_genotype_snp mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

# 5 partitions:
# Time: 1830605.600 ms (30:30.606)
# Time: 1832960.147 ms (30:32.960)
# Time: 1834207.072 ms (30:34.207)
# Time: 1838887.626 ms (30:38.888)
# Time: 1840289.062 ms (30:40.289)
#
# Time: 1830408.737 ms (30:30.409)
# Time: 1836844.268 ms (30:36.844)
# Time: 1837653.286 ms (30:37.653)
# Time: 1838629.513 ms (30:38.630)
# Time: 1838785.616 ms (30:38.786)
