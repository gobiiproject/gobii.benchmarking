#!/bin/sh
###### All markers x N samples contiguous #########

# Set the third parameter of parse_sample_range() to the desired number of samples.

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_indel mgs
             where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_indel mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_indel mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_indel mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_indel mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

### 50 samples:
# Time: 1520209.054 ms (25:20.209)
# Time: 1520205.178 ms (25:20.205)
# Time: 1521555.230 ms (25:21.555)
# Time: 1523558.690 ms (25:23.559)
# Time: 1524989.141 ms (25:24.989)
#
# Time: 1512493.577 ms (25:12.494)
# Time: 1517820.339 ms (25:17.820)
# Time: 1518554.178 ms (25:18.554)
# Time: 1519066.538 ms (25:19.067)
# Time: 1523927.041 ms (25:23.927)

### 100 samples
# Time: 1559623.669 ms (25:59.624)
# Time: 1560391.625 ms (26:00.392)
# Time: 1560871.830 ms (26:00.872)
# Time: 1562563.439 ms (26:02.563)
# Time: 1563194.468 ms (26:03.194)
#
# Time: 1560603.939 ms (26:00.604)
# Time: 1563553.133 ms (26:03.553)
# Time: 1565449.847 ms (26:05.450)
# Time: 1566515.108 ms (26:06.515)
# Time: 1567777.329 ms (26:07.777)

### 500 samples
# Time: 1868588.093 ms (31:08.588)
# Time: 1872869.220 ms (31:12.869)
# Time: 1874507.732 ms (31:14.508)
# Time: 1875097.202 ms (31:15.097)
# Time: 1877659.680 ms (31:17.660)
#
# Time: 1868326.306 ms (31:08.326)
# Time: 1869640.116 ms (31:09.640)
# Time: 1875737.559 ms (31:15.738)
# Time: 1877542.353 ms (31:17.542)
# Time: 1878500.869 ms (31:18.501)

### 1000 samples
# Time: 2268503.035 ms (37:48.503)
# Time: 2272335.296 ms (37:52.335)
# Time: 2275025.502 ms (37:55.026)
# Time: 2275579.350 ms (37:55.579)
# Time: 2278304.258 ms (37:58.304)
#
# Time: 2274705.284 ms (37:54.705)
# Time: 2277941.245 ms (37:57.941)
# Time: 2280461.707 ms (38:00.462)
# Time: 2280641.565 ms (38:00.642)
# Time: 2282432.323 ms (38:02.432)

### 2000 samples
# Time: 3067149.302 ms (51:07.149)
# Time: 3080530.744 ms (51:20.531)
# Time: 3084399.239 ms (51:24.399)
# Time: 3093902.631 ms (51:33.903)
# Time: 3093956.946 ms (51:33.957)
#
# Time: 3068540.504 ms (51:08.541)
# Time: 3085840.284 ms (51:25.840)
# Time: 3088551.986 ms (51:28.552)
# Time: 3088595.754 ms (51:28.596)
# Time: 3092474.965 ms (51:32.475)

### 3000 samples
# Time: 3881379.258 ms (01:04:41.379)
# Time: 3886046.906 ms (01:04:46.047)
# Time: 3887702.342 ms (01:04:47.702)
# Time: 3893598.599 ms (01:04:53.599)
# Time: 3902426.881 ms (01:05:02.427)
#
# Time: 3881782.444 ms (01:04:41.782)
# Time: 3883327.006 ms (01:04:43.327)
# Time: 3885223.908 ms (01:04:45.224)
# Time: 3892260.547 ms (01:04:52.261)
# Time: 3900167.833 ms (01:05:00.168)
