#!/bin/sh
###### All markers x N samples contiguous #########

# Set the third parameter of parse_sample_range() to the desired number of samples.

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_tetraploid mgs
             where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_tetraploid mgs
             where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_tetraploid mgs
             where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_C.csv' with CSV DELIMITER E'\t';" &

#
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_tetraploid mgs
             where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
	     from marker_genotype_tetraploid mgs
             where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_2kSamples_ctg_E.csv' with CSV DELIMITER E'\t';" &

### 50 samples:
# Time: 1584687.218 ms (26:24.687)
# Time: 1587058.963 ms (26:27.059)
# Time: 1588681.886 ms (26:28.682)
# Time: 1589990.851 ms (26:29.991)
# Time: 1591435.712 ms (26:31.436)
#
# Time: 1585338.770 ms (26:25.339)
# Time: 1587458.083 ms (26:27.458)
# Time: 1588262.843 ms (26:28.263)
# Time: 1588729.959 ms (26:28.730)
# Time: 1590949.648 ms (26:30.950)

### 100 samples:
# Time: 1622809.852 ms (27:02.810)
# Time: 1631546.437 ms (27:11.546)
# Time: 1632002.783 ms (27:12.003)
# Time: 1632078.250 ms (27:12.078)
# Time: 1633198.911 ms (27:13.199)
#
# Time: 1628432.014 ms (27:08.432)
# Time: 1629536.954 ms (27:09.537)
# Time: 1629650.957 ms (27:09.651)
# Time: 1630050.874 ms (27:10.051)
# Time: 1631324.883 ms (27:11.325)

### 500 samples
# Time: 1953950.131 ms (32:33.950)
# Time: 1964438.147 ms (32:44.438)
# Time: 1966134.430 ms (32:46.134)
# Time: 1968905.970 ms (32:48.906)
# Time: 1971915.037 ms (32:51.915)
#
# Time: 1966683.410 ms (32:46.683)
# Time: 1967987.764 ms (32:47.988)
# Time: 1971168.298 ms (32:51.168)
# Time: 1972826.935 ms (32:52.827)

### 1000 samples
# Time: 2398459.513 ms (39:58.460)
# Time: 2402345.165 ms (40:02.345)
# Time: 2407615.540 ms (40:07.616)
# Time: 2412217.856 ms (40:12.218)
# Time: 2412229.884 ms (40:12.230)
#
# Time: 2388573.707 ms (39:48.574)
# Time: 2391486.920 ms (39:51.487)
# Time: 2401225.331 ms (40:01.225)
# Time: 2415007.858 ms (40:15.008)
# Time: 2415685.913 ms (40:15.686)

### 2000 samples
# Time: 3253369.951 ms (54:13.370)
# Time: 3258564.855 ms (54:18.565)
# Time: 3265898.287 ms (54:25.898)
# Time: 3272992.976 ms (54:32.993)
# Time: 3278691.608 ms (54:38.692)
#
# Time: 3250067.596 ms (54:10.068)
# Time: 3256099.489 ms (54:16.099)
# Time: 3272568.332 ms (54:32.568)
# Time: 3272957.371 ms (54:32.957)
# Time: 3278243.581 ms (54:38.244)

### 3000 samples
# Time: 4147359.384 ms (01:09:07.359)
# Time: 4147995.046 ms (01:09:07.995)
# Time: 4169433.575 ms (01:09:29.434)
# Time: 4178136.068 ms (01:09:38.136)
# Time: 4180805.886 ms (01:09:40.806)
#
# Time: 4146991.389 ms (01:09:06.991)
# Time: 4156268.826 ms (01:09:16.269)
# Time: 4164085.124 ms (01:09:24.085)
# Time: 4164369.900 ms (01:09:24.370)
# Time: 4189732.574 ms (01:09:49.733)
