#!/bin/sh
###### All markers x N samples non-contiguous #########
# Divide the table into five partitions.

# Before running this script, load table samples_input with the list of samples to be queried, as follows:
# -- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
# -- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
# truncate samples_input;
# copy public.samples_input (sample_id)
# FROM PROGRAM 'cat /data/gobii.benchmarking/randomlists/SNPs_only/pg_samples.3000 | tr "\n" "\t"'
# DELIMITER E',';

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, samples_input si
where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, samples_input si
where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, samples_input si
where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, samples_input si
where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, samples_input si
where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_E.csv' with CSV DELIMITER E'\t';" &

### 50 samples:
# Time: 1503078.898 ms (25:03.079)
# Time: 1507245.523 ms (25:07.246)
# Time: 1508990.438 ms (25:08.990)
# Time: 1510106.471 ms (25:10.106)
# Time: 1512942.332 ms (25:12.942)
#
# Time: 1505399.727 ms (25:05.400)
# Time: 1507102.152 ms (25:07.102)
# Time: 1510195.805 ms (25:10.196)
# Time: 1510213.165 ms (25:10.213)
# Time: 1510482.054 ms (25:10.482)

### 100 samples:
# Time: 1530477.325 ms (25:30.477)
# Time: 1530574.458 ms (25:30.574)
# Time: 1533065.879 ms (25:33.066)
# Time: 1535311.779 ms (25:35.312)
# Time: 1535672.813 ms (25:35.673)
#
# Time: 1528258.002 ms (25:28.258)
# Time: 1531963.617 ms (25:31.964)
# Time: 1532236.769 ms (25:32.237)
# Time: 1535149.276 ms (25:35.149)
# Time: 1535395.873 ms (25:35.396)

### 500 samples:
# Time: 1738979.548 ms (28:58.980)
# Time: 1740051.987 ms (29:00.052)
# Time: 1740632.608 ms (29:00.633)
# Time: 1743885.764 ms (29:03.886)
# Time: 1744696.363 ms (29:04.696)
#
# Time: 1743435.829 ms (29:03.436)
# Time: 1747259.044 ms (29:07.259)
# Time: 1747735.783 ms (29:07.736)
# Time: 1748509.242 ms (29:08.509)
# Time: 1749116.191 ms (29:09.116)

### 1000 samples:
# Time: 2001508.482 ms (33:21.508)
# Time: 2003797.043 ms (33:23.797)
# Time: 2007907.952 ms (33:27.908)
# Time: 2008145.409 ms (33:28.145)
# Time: 2009093.211 ms (33:29.093)
#
# Time: 2004099.231 ms (33:24.099)
# Time: 2004518.359 ms (33:24.518)
# Time: 2004598.347 ms (33:24.598)
# Time: 2005531.512 ms (33:25.532)
# Time: 2007711.201 ms (33:27.711)

### 2000 samples:
# Time: 2515333.075 ms (41:55.333)
# Time: 2516744.098 ms (41:56.744)
# Time: 2518838.980 ms (41:58.839)
# Time: 2520271.583 ms (42:00.272)
# Time: 2524092.420 ms (42:04.092)
#
# Time: 2518114.000 ms (41:58.114)
# Time: 2522618.862 ms (42:02.619)
# Time: 2522901.474 ms (42:02.901)
# Time: 2524148.196 ms (42:04.148)
# Time: 2534135.980 ms (42:14.136)

### 3000 samples:
# Time: 3013343.390 ms (50:13.343)
# Time: 3022696.152 ms (50:22.696)
# Time: 3022707.888 ms (50:22.708)
# Time: 3027578.264 ms (50:27.578)
# Time: 3033583.923 ms (50:33.584)
#
# Time: 3016078.840 ms (50:16.079)
# Time: 3026929.106 ms (50:26.929)
# Time: 3028217.832 ms (50:28.218)
# Time: 3034205.103 ms (50:34.205)
