#!/bin/sh
###### All markers x N samples non-contiguous #########
# Divide the table into five partitions.

# -- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
# -- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
# truncate samples_input;
# copy public.samples_input (sample_id)
# FROM PROGRAM 'cat /data/gobii.benchmarking/randomlists/SNPs_only/pg_samples.3000 | tr "\n" "\t"'
# DELIMITER E',';

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si
where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_E.csv' with CSV DELIMITER E'\t';" &

### 50 samples:
# Time: 1476587.592 ms (24:36.588)
# Time: 1483226.203 ms (24:43.226)
# Time: 1484498.780 ms (24:44.499)
# Time: 1485799.712 ms (24:45.800)
# Time: 1486937.385 ms (24:46.937)
#
# Time: 1480595.757 ms (24:40.596)
# Time: 1480983.065 ms (24:40.983)
# Time: 1482140.168 ms (24:42.140)
# Time: 1483994.574 ms (24:43.995)
# Time: 1485580.202 ms (24:45.580)

### 100 samples:
# Time: 1499167.678 ms (24:59.168)
# Time: 1500181.643 ms (25:00.182)
# Time: 1503440.023 ms (25:03.440)
# Time: 1506210.531 ms (25:06.211)
# Time: 1509295.556 ms (25:09.296)
#
# Time: 1503873.209 ms (25:03.873)
# Time: 1505042.127 ms (25:05.042)
# Time: 1507455.911 ms (25:07.456)
# Time: 1508087.790 ms (25:08.088)
# Time: 1508689.640 ms (25:08.690)

### 500 samples:
# Time: 1700898.202 ms (28:20.898)
# Time: 1702885.884 ms (28:22.886)
# Time: 1704006.652 ms (28:24.007)
# Time: 1704512.316 ms (28:24.512)
# Time: 1707511.435 ms (28:27.511)
#
# Time: 1701371.002 ms (28:21.371)
# Time: 1704725.954 ms (28:24.726)
# Time: 1704923.937 ms (28:24.924)
# Time: 1705015.883 ms (28:25.016)
# Time: 1709939.146 ms (28:29.939)

### 1000 samples:
# Time: 1945577.158 ms (32:25.577)
# Time: 1945791.486 ms (32:25.791)
# Time: 1946257.008 ms (32:26.257)
# Time: 1949865.947 ms (32:29.866)
# Time: 1955690.271 ms (32:35.690)
#
# Time: 1953196.945 ms (32:33.197)
# Time: 1954041.794 ms (32:34.042)
# Time: 1955218.644 ms (32:35.219)
# Time: 1955547.981 ms (32:35.548)
# Time: 1955945.749 ms (32:35.946)

### 2000, see allMarkers_2kSamples_nonctg_snps_5parts.sh.

### 3000 samples:
# Time: 2884633.433 ms (48:04.633)
# Time: 2885316.865 ms (48:05.317)
# Time: 2886819.669 ms (48:06.820)
# Time: 2891364.574 ms (48:11.365)
# Time: 2896665.259 ms (48:16.665)
#
# Time: 2889868.721 ms (48:09.869)
# Time: 2894163.727 ms (48:14.164)
# Time: 2894784.504 ms (48:14.785)
# Time: 2895478.553 ms (48:15.479)
# Time: 2897849.861 ms (48:17.850)
