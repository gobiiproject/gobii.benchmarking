#!/bin/sh
###### All markers x N samples non-contiguous #########
# Divide the table into five partitions.

# Before running this script, load table samples_input with the list of samples to be queried, as follows:
# -- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
# -- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
# truncate samples_input;
# copy public.samples_input (sample_id)
# FROM PROGRAM 'cat /data/gobii.benchmarking/randomlists/SNPs_only/pg_samples.3000 | tr "\n" "\t"'
# DELIMITER E',';

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_tetraploid mgs, samples_input si
where mgs.marker_genotype_id <= 4000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_tetraploid mgs, samples_input si
where mgs.marker_genotype_id > 4000000 and mgs.marker_genotype_id <= 8000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_tetraploid mgs, samples_input si
where mgs.marker_genotype_id > 8000000 and mgs.marker_genotype_id <= 12000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_tetraploid mgs, samples_input si
where mgs.marker_genotype_id > 12000000 and mgs.marker_genotype_id <= 16000000)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_tetraploid mgs, samples_input si
where mgs.marker_genotype_id > 16000000 and mgs.marker_genotype_id <= 20000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_3kSamples_E.csv' with CSV DELIMITER E'\t';" &

### 100 samples:
# Time: 1596790.718 ms (26:36.791)
# Time: 1598361.550 ms (26:38.362)
# Time: 1598829.790 ms (26:38.830)
# Time: 1602862.219 ms (26:42.862)
# Time: 1603710.713 ms (26:43.711)
#
# Time: 1602712.924 ms (26:42.713)
# Time: 1604532.057 ms (26:44.532)
# Time: 1605253.135 ms (26:45.253)
# Time: 1606469.726 ms (26:46.470)
# Time: 1609068.763 ms (26:49.069)

### 50 samples:
# Time: 1567308.184 ms (26:07.308)
# Time: 1574624.576 ms (26:14.625)
# Time: 1574628.401 ms (26:14.628)
# Time: 1575147.757 ms (26:15.148)
# Time: 1575580.453 ms (26:15.580)
#
# Time: 1572200.814 ms (26:12.201)
# Time: 1575300.119 ms (26:15.300)
# Time: 1576026.015 ms (26:16.026)
# Time: 1576990.997 ms (26:16.991)
# Time: 1578881.448 ms (26:18.881)

### 500 samples:
# Time: 1831129.336 ms (30:31.129)
# Time: 1831461.819 ms (30:31.462)
# Time: 1833352.679 ms (30:33.353)
# Time: 1837636.202 ms (30:37.636)
# Time: 1838312.431 ms (30:38.312)
#
# Time: 1836296.391 ms (30:36.296)
# Time: 1837193.951 ms (30:37.194)
# Time: 1837409.439 ms (30:37.409)
# Time: 1839318.567 ms (30:39.319)
# Time: 1840118.755 ms (30:40.119)

### 1000 samples:
# Time: 2115452.176 ms (35:15.452)
# Time: 2117909.337 ms (35:17.909)
# Time: 2123172.408 ms (35:23.172)
# Time: 2134920.992 ms (35:34.921)
# Time: 2148750.315 ms (35:48.750)
#
# Time: 2119443.181 ms (35:19.443)
# Time: 2127765.460 ms (35:27.765)
# Time: 2128325.447 ms (35:28.325)
# Time: 2141168.083 ms (35:41.168)
# Time: 2141245.721 ms (35:41.246)

### 2000 samples:
# Time: 2697647.005 ms (44:57.647)
# Time: 2706286.660 ms (45:06.287)
# Time: 2722583.876 ms (45:22.584)
# Time: 2727832.756 ms (45:27.833)
# Time: 2732144.279 ms (45:32.144)
#
# Time: 2677508.399 ms (44:37.508)
# Time: 2696258.884 ms (44:56.259)
# Time: 2701745.786 ms (45:01.746)
# Time: 2705838.496 ms (45:05.838)
# Time: 2706325.702 ms (45:06.326)

### 3000 samples:
# Time: 3272986.313 ms (54:32.986)
# Time: 3285164.250 ms (54:45.164)
# Time: 3292988.496 ms (54:52.988)
# Time: 3304075.042 ms (55:04.075)
# Time: 3313853.558 ms (55:13.854)
#
# Time: 3225629.173 ms (53:45.629)
# Time: 3232183.816 ms (53:52.184)
# Time: 3292572.826 ms (54:52.573)
# Time: 3308751.817 ms (55:08.752)
# Time: 3314279.276 ms (55:14.279)
