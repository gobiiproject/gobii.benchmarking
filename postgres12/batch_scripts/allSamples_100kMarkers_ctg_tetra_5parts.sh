#!/bin/sh

# All samples, N markers contiguous.  SNPs+Indels+Tetraploids dataset
# 100,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 100100 and marker_genotype_id >= 80100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 80100 and marker_genotype_id >=60100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 60100 and marker_genotype_id >= 40100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 40100 and marker_genotype_id >= 20100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 20100 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_e.csv' with CSV DELIMITER E'\t';" &

# 100,000 markers:
# Time: 52226.202 ms (00:52.226)
# Time: 53274.018 ms (00:53.274)
# Time: 53411.329 ms (00:53.411)
# Time: 53852.538 ms (00:53.853)
# Time: 61242.498 ms (01:01.242)
#
# Time: 51083.862 ms (00:51.084)
# Time: 51141.473 ms (00:51.141)
# Time: 53255.310 ms (00:53.255)
# Time: 53400.588 ms (00:53.401)
# Time: 58017.018 ms (00:58.017)
#
# Time: 50786.118 ms (00:50.786)
# Time: 51197.643 ms (00:51.198)
# Time: 52358.294 ms (00:52.358)
# Time: 55753.100 ms (00:55.753)
# Time: 56118.393 ms (00:56.118)
