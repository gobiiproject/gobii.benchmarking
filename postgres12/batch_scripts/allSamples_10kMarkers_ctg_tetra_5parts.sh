#!/bin/sh

# All samples, N markers contiguous.  SNPs+Indels+Tetraploids dataset
# 10,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 10100 and marker_genotype_id >= 8100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 8100 and marker_genotype_id >=6100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 6100 and marker_genotype_id >= 4100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 4100 and marker_genotype_id >= 2100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 2100 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_e.csv' with CSV DELIMITER E'\t';" &

# 10,000 markers
# Time: 5401.220 ms (00:05.401)
# Time: 5410.711 ms (00:05.411)
# Time: 5556.879 ms (00:05.557)
# Time: 6007.640 ms (00:06.008)
# Time: 6639.266 ms (00:06.639)
#
# Time: 5465.666 ms (00:05.466)
# Time: 5506.614 ms (00:05.507)
# Time: 5614.709 ms (00:05.615)
# Time: 5906.555 ms (00:05.907)
# Time: 6418.339 ms (00:06.418)
#
# Time: 5274.845 ms (00:05.275)
# Time: 5302.825 ms (00:05.303)
# Time: 5390.455 ms (00:05.390)
# Time: 5405.762 ms (00:05.406)
# Time: 6587.800 ms (00:06.588)
