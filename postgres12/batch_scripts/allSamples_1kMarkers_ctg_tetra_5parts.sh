#!/bin/sh

# All samples, N markers contiguous.  SNPs+Indels+Tetraploids dataset
# 1,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 1100 and marker_genotype_id >= 900)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 900 and marker_genotype_id >= 700)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 700 and marker_genotype_id >= 500)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 500 and marker_genotype_id >= 300)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 300 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_e.csv' with CSV DELIMITER E'\t';" &

# 1000 markers:
# Time: 1112.787 ms (00:01.113)
# Time: 1130.605 ms (00:01.131)
# Time: 1164.809 ms (00:01.165)
# Time: 1173.131 ms (00:01.173)
# Time: 1192.430 ms (00:01.192)
#
# Time: 721.457 ms
# Time: 771.140 ms
# Time: 782.169 ms
# Time: 807.622 ms
# Time: 808.453 ms
#
# Time: 678.590 ms
# Time: 766.236 ms
# Time: 772.458 ms
# Time: 776.789 ms
# Time: 785.171 ms
