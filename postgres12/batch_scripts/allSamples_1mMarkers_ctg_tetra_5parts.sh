#!/bin/sh

# All samples, N markers contiguous.  SNPs+Indels+Tetraploids dataset
# 1,000,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 1000100 and marker_genotype_id >= 800100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 800100 and marker_genotype_id >= 600100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 600100 and marker_genotype_id >= 400100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 400100 and marker_genotype_id >= 200100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 200100 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_e.csv' with CSV DELIMITER E'\t';" &

# 1,000,000 markers:
# Time: 181361.709 ms (03:01.362)
# Time: 230211.784 ms (03:50.212)
# Time: 232135.848 ms (03:52.136)
# Time: 232747.673 ms (03:52.748)
# Time: 233085.318 ms (03:53.085)

# Time: 184219.196 ms (03:04.219)
# Time: 233906.602 ms (03:53.907)
# Time: 234639.830 ms (03:54.640)
# Time: 235952.656 ms (03:55.953)
# Time: 235999.461 ms (03:55.999)

# Time: 181739.984 ms (03:01.740)
# Time: 230828.622 ms (03:50.829)
# Time: 231531.276 ms (03:51.531)
# Time: 235675.959 ms (03:55.676)
# Time: 235990.044 ms (03:55.990)

