#!/bin/sh

# All samples, N markers contiguous.  SNPs+Indels+Tetraploid dataset
# 5,000,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 5000100 and marker_genotype_id >= 4000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 4000100 and marker_genotype_id >= 3000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 3000100 and marker_genotype_id >= 2000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 2000100 and marker_genotype_id >= 1000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 1000100 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_e.csv' with CSV DELIMITER E'\t';" &

# 5,000,000 markers:
# Time: 1037374.811 ms (17:17.375)
# Time: 1061682.811 ms (17:41.683)
# Time: 1069872.411 ms (17:49.872)
# Time: 1075451.150 ms (17:55.451)
# Time: 1083220.832 ms (18:03.221)
#
# Time: 1039110.531 ms (17:19.111)
# Time: 1040898.236 ms (17:20.898)
# Time: 1073737.621 ms (17:53.738)
# Time: 1086188.237 ms (18:06.188)
# Time: 1096771.301 ms (18:16.771)
#
# Time: 1039367.982 ms (17:19.368)
# Time: 1045817.197 ms (17:25.817)
# Time: 1076356.030 ms (17:56.356)
# Time: 1085227.998 ms (18:05.228)
# Time: 1096275.863 ms (18:16.276)

