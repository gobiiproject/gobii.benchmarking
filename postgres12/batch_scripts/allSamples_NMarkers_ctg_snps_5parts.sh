#!/bin/sh

# All samples, N markers contiguous.  SNPs-only dataset
# 5,000,000 markers

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 5000100 and marker_genotype_id >= 4000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 4000100 and marker_genotype_id >= 3000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 3000100 and marker_genotype_id >= 2000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 2000100 and marker_genotype_id >= 1000100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 1000100 and marker_genotype_id >= 100)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_ctg_a.csv' with CSV DELIMITER E'\t';" &


# 100,000 markers:
# Time: 48036.526 ms (00:48.037)
# Time: 48314.867 ms (00:48.315)
# Time: 49874.461 ms (00:49.874)
# Time: 52042.105 ms (00:52.042)
# Time: 52497.643 ms (00:52.498)
#
# Time: 47724.402 ms (00:47.724)
# Time: 47783.417 ms (00:47.783)
# Time: 49740.015 ms (00:49.740)
# Time: 51411.019 ms (00:51.411)
# Time: 52260.635 ms (00:52.261)
#
# Time: 48037.952 ms (00:48.038)
# Time: 48484.649 ms (00:48.485)
# Time: 50184.673 ms (00:50.185)
# Time: 50269.461 ms (00:50.269)
# Time: 54174.269 ms (00:54.174)

# 1,000,000 markers:
# Time: 147073.529 ms (02:27.074)
# Time: 192212.915 ms (03:12.213)
# Time: 192814.948 ms (03:12.815)
# Time: 193087.922 ms (03:13.088)
# Time: 194289.146 ms (03:14.289)
#
# Time: 148028.072 ms (02:28.028)
# Time: 192835.597 ms (03:12.836)
# Time: 193114.759 ms (03:13.115)
# Time: 193362.448 ms (03:13.362)
# Time: 193463.527 ms (03:13.464)
#
# Time: 147514.893 ms (02:27.515)
# Time: 192484.138 ms (03:12.484)
# Time: 192577.504 ms (03:12.578)
# Time: 193105.416 ms (03:13.105)
# Time: 193121.143 ms (03:13.121)

# 5,000,000 markers:
# Time: 936957.459 ms (15:36.957)
# Time: 953675.876 ms (15:53.676)
# Time: 954854.692 ms (15:54.855)
# Time: 954854.368 ms (15:54.854)
# Time: 962227.207 ms (16:02.227)
#
# Time: 945433.268 ms (15:45.433)
# Time: 952356.009 ms (15:52.356)
# Time: 954233.193 ms (15:54.233)
# Time: 956211.047 ms (15:56.211)
# Time: 956290.990 ms (15:56.291)
#
# Time: 947449.316 ms (15:47.449)
# Time: 952949.715 ms (15:52.950)
# Time: 953888.509 ms (15:53.889)
# Time: 956157.189 ms (15:56.157)
# Time: 958967.525 ms (15:58.968)
