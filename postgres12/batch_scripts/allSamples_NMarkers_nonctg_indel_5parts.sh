#!/bin/sh

# First load the markers_input_* tables with the appropriate marker lists using ./split_marker_lists_indel.sh, then run this script.
# If desired, FIRST build new randomized marker lists using ../../randomlists/selectmarkers.

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs, markers_input_a mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs, markers_input_b mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs, markers_input_c mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs, markers_input_d mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs, markers_input_e mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_e.csv' with CSV DELIMITER E'\t';" &

# 100,000 markers, with a new list of markers for each run:
# Time: 426484.475 ms (07:06.484)
# Time: 426602.081 ms (07:06.602)
# Time: 428226.025 ms (07:08.226)
# Time: 429660.266 ms (07:09.660)
# Time: 429763.790 ms (07:09.764)
#
# Time: 281805.288 ms (04:41.805)
# Time: 284631.865 ms (04:44.632)
# Time: 284941.761 ms (04:44.942)
# Time: 285326.478 ms (04:45.326)
# Time: 285492.538 ms (04:45.493)
#
# Time: 263555.700 ms (04:23.556)
# Time: 264911.340 ms (04:24.911)
# Time: 266437.144 ms (04:26.437)
# Time: 266885.018 ms (04:26.885)
# Time: 268172.026 ms (04:28.172)
#
# 3rd one again:
# Time: 50160.112 ms (00:50.160)
# Time: 50319.837 ms (00:50.320)
# Time: 51400.743 ms (00:51.401)
# Time: 51445.239 ms (00:51.445)
# Time: 55733.217 ms (00:55.733)
#
# 3rd one again:
# Time: 49932.920 ms (00:49.933)
# Time: 49966.076 ms (00:49.966)
# Time: 52506.077 ms (00:52.506)
# Time: 52506.860 ms (00:52.507)
# Time: 58501.832 ms (00:58.502)
#
# 3rd one again:
# Time: 49677.765 ms (00:49.678)
# Time: 49795.649 ms (00:49.796)
# Time: 50810.361 ms (00:50.810)
# Time: 51504.292 ms (00:51.504)
# Time: 56574.181 ms (00:56.574)

# 1,000,000 markers, with a new list of markers for each run::
# Time: 1479510.371 ms (24:39.510)
# Time: 1480698.938 ms (24:40.699)
# Time: 1482227.665 ms (24:42.228)
# Time: 1484813.732 ms (24:44.814)
# Time: 1488839.059 ms (24:48.839)
#
# Time: 1374377.650 ms (22:54.378)
# Time: 1374916.884 ms (22:54.917)
# Time: 1378383.543 ms (22:58.384)
# Time: 1380900.561 ms (23:00.901)
# Time: 1391414.273 ms (23:11.414)
#
# Time: 1317794.888 ms (21:57.795)
# Time: 1318610.429 ms (21:58.610)
# Time: 1318732.580 ms (21:58.733)
# Time: 1319074.357 ms (21:59.074)
# Time: 1340057.398 ms (22:20.057)
#
# 3rd one again:
# Time: 298093.943 ms (04:58.094)
# Time: 300774.604 ms (05:00.775)
# Time: 300952.961 ms (05:00.953)
# Time: 302564.677 ms (05:02.565)
# Time: 302765.405 ms (05:02.765)
#
# 3rd one again:
# Time: 288735.812 ms (04:48.736)
# Time: 289028.259 ms (04:49.028)
# Time: 289195.991 ms (04:49.196)
# Time: 290817.208 ms (04:50.817)
# Time: 292212.335 ms (04:52.212)
#
# 3rd one again:
# Time: 289298.825 ms (04:49.299)
# Time: 290040.941 ms (04:50.041)
# Time: 290583.261 ms (04:50.583)
# Time: 291302.717 ms (04:51.303)
# Time: 291821.016 ms (04:51.821)

# 5,000,000 markers, with a new list of markers for each run::
# Time: 1070529.559 ms (17:50.530)
# Time: 1070560.318 ms (17:50.560)
# Time: 1070611.297 ms (17:50.611)
# Time: 1070621.072 ms (17:50.621)
# Time: 1070702.554 ms (17:50.703)
#
# Time: 1083738.590 ms (18:03.739)
# Time: 1083772.991 ms (18:03.773)
# Time: 1083827.472 ms (18:03.827)
# Time: 1083871.654 ms (18:03.872)
# Time: 1083899.127 ms (18:03.899)
#
# Time: 1093758.780 ms (18:13.759)
# Time: 1093759.269 ms (18:13.759)
# Time: 1093779.856 ms (18:13.780)
# Time: 1093802.514 ms (18:13.803)
# Time: 1093813.988 ms (18:13.814)
#
# 3rd one again:
# Time: 1123328.621 ms (18:43.329)
# Time: 1123383.657 ms (18:43.384)
# Time: 1123410.613 ms (18:43.411)
# Time: 1123437.153 ms (18:43.437)
# Time: 1123445.162 ms (18:43.445)
#
# 3rd one again:
# Time: 1112275.344 ms (18:32.275)
# Time: 1112341.472 ms (18:32.341)
# Time: 1112345.237 ms (18:32.345)
# Time: 1112368.339 ms (18:32.368)
# Time: 1112375.366 ms (18:32.375)
#
# 3rd one again:
# Time: 1112566.716 ms (18:32.567)
# Time: 1112573.111 ms (18:32.573)
# Time: 1112577.646 ms (18:32.578)
# Time: 1112589.564 ms (18:32.590)
# Time: 1112603.082 ms (18:32.603)
