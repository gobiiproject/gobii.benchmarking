#!/bin/sh

# First load the markers_input_* tables with the appropriate marker lists using ./split_marker_lists_snp.sh, then run this script.
# If desired, FIRST build new randomized marker lists using ../../randomlists/selectmarkers.

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_a mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_b mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_c mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_d mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_e mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_e.csv' with CSV DELIMITER E'\t';" &

# 1000 markers:
# Time: 617.253 ms
# Time: 695.734 ms
# Time: 702.402 ms
# Time: 774.321 ms
# Time: 769.738 ms
#
# Time: 607.612 ms
# Time: 628.295 ms
# Time: 658.528 ms
# Time: 716.978 ms
# Time: 759.650 ms

# 10,000 markers:
# Time: 5028.286 ms (00:05.028)
# Time: 5232.229 ms (00:05.232)
# Time: 5276.950 ms (00:05.277)
# Time: 5288.495 ms (00:05.288)
# Time: 5637.706 ms (00:05.638)
#
# Time: 5022.648 ms (00:05.023)
# Time: 5034.725 ms (00:05.035)
# Time: 5610.476 ms (00:05.610)
# Time: 5850.658 ms (00:05.851)
# Time: 6306.285 ms (00:06.306)
#
# Time: 5118.357 ms (00:05.118)
# Time: 5132.172 ms (00:05.132)
# Time: 5183.618 ms (00:05.184)
# Time: 5381.140 ms (00:05.381)
# Time: 6026.943 ms (00:06.027)

# 100,000 markers:
# Time: 48746.660 ms (00:48.747)
# Time: 49430.106 ms (00:49.430)
# Time: 51164.571 ms (00:51.165)
# Time: 51416.617 ms (00:51.417)
# Time: 53274.501 ms (00:53.275)
#
# Time: 48801.097 ms (00:48.801)
# Time: 49070.103 ms (00:49.070)
# Time: 50317.918 ms (00:50.318)
# Time: 53643.222 ms (00:53.643)
# Time: 55115.536 ms (00:55.116)
#
# Time: 49046.101 ms (00:49.046)
# Time: 49070.355 ms (00:49.070)
# Time: 50755.144 ms (00:50.755)
# Time: 52904.226 ms (00:52.904)
# Time: 54961.739 ms (00:54.962)

# 1,000,000 markers:
# Time: 1267044.134 ms (21:07.044)
# Time: 1268170.280 ms (21:08.170)
# Time: 1270967.595 ms (21:10.968)
# Time: 1275306.203 ms (21:15.306)
# Time: 1275907.244 ms (21:15.907)
#
# Time: 287876.725 ms (04:47.877)
# Time: 289024.884 ms (04:49.025)
# Time: 290252.908 ms (04:50.253)
# Time: 290769.376 ms (04:50.769)
# Time: 291123.752 ms (04:51.124)
#
# Time: 273634.826 ms (04:33.635)
# Time: 274069.567 ms (04:34.070)
# Time: 274839.208 ms (04:34.839)
# Time: 275877.610 ms (04:35.878)
# Time: 276466.863 ms (04:36.467)


# 5,000,000 markers:
# ERROR:  could not resize shared memory segment "/PostgreSQL.1460248698" to 2097152 bytes: No space left on device

# With more shared memory:
# Time: 4873656.707 ms (01:21:13.657)
# Time: 4878771.576 ms (01:21:18.772)
# Time: 4882473.070 ms (01:21:22.473)
# Time: 4884294.300 ms (01:21:24.294)
# Time: 4896478.983 ms (01:21:36.479)
#
# Time: 1043175.744 ms (17:23.176)
# Time: 1043179.171 ms (17:23.179)
# Time: 1043213.340 ms (17:23.213)
# Time: 1043224.695 ms (17:23.225)
# Time: 1043290.502 ms (17:23.291)
#
# Time: 1059643.469 ms (17:39.643)
# Time: 1059677.209 ms (17:39.677)
# Time: 1059698.927 ms (17:39.699)
# Time: 1059698.979 ms (17:39.699)
# Time: 1059717.163 ms (17:39.717)
#
# Time: 1063497.752 ms (17:43.498)
# Time: 1063529.395 ms (17:43.529)
# Time: 1063533.939 ms (17:43.534)
# Time: 1063543.410 ms (17:43.543)
# Time: 1063556.006 ms (17:43.556)

# 10oct20: Repeat 5m.
# Time: 1060002.930 ms (17:40.003)
# Time: 1060017.743 ms (17:40.018)
# Time: 1060069.827 ms (17:40.070)
# Time: 1060074.172 ms (17:40.074)
# Time: 1060095.111 ms (17:40.095)
#
# Time: 1066579.565 ms (17:46.580)
# Time: 1066579.952 ms (17:46.580)
# Time: 1066599.456 ms (17:46.599)
# Time: 1066600.402 ms (17:46.600)
# Time: 1066616.658 ms (17:46.617)
#
# Time: 1064978.339 ms (17:44.978)
# Time: 1064979.824 ms (17:44.980)
# Time: 1065017.999 ms (17:45.018)
# Time: 1065023.851 ms (17:45.024)
# Time: 1065076.244 ms (17:45.076)

# 1m with a new list of markers for each run:
# Time: 1486304.096 ms (24:46.304)
# Time: 1487975.387 ms (24:47.975)
# Time: 1488010.279 ms (24:48.010)
# Time: 1489623.247 ms (24:49.623)
# Time: 1503052.582 ms (25:03.053)
#
# Time: 1288927.559 ms (21:28.928)
# Time: 1291410.461 ms (21:31.410)
# Time: 1301208.888 ms (21:41.209)
# Time: 1302373.769 ms (21:42.374)
# Time: 1308386.571 ms (21:48.387)
#
# Time: 1231937.944 ms (20:31.938)
# Time: 1232218.485 ms (20:32.218)
# Time: 1234460.585 ms (20:34.461)
# Time: 1243941.558 ms (20:43.942)
# Time: 1249933.430 ms (20:49.933)
#
# 3rd one again:
# Time: 282938.687 ms (04:42.939)
# Time: 283647.014 ms (04:43.647)
# Time: 283980.003 ms (04:43.980)
# Time: 286461.034 ms (04:46.461)
# Time: 286471.480 ms (04:46.471)
#
# 3rd one again:
# Time: 273421.113 ms (04:33.421)
# Time: 273491.774 ms (04:33.492)
# Time: 274059.136 ms (04:34.059)
# Time: 275179.002 ms (04:35.179)
# Time: 277504.780 ms (04:37.505)
#
# 3rd one again:
# Time: 274613.377 ms (04:34.613)
# Time: 275168.942 ms (04:35.169)
# Time: 275351.111 ms (04:35.351)
# Time: 275782.247 ms (04:35.782)
# Time: 277713.023 ms (04:37.713)

# 5m with a new list of markers for each run:
# Time: 1063413.940 ms (17:43.414)
# Time: 1063419.533 ms (17:43.420)
# Time: 1063440.828 ms (17:43.441)
# Time: 1063440.796 ms (17:43.441)
# Time: 1063457.144 ms (17:43.457)
#
# Time: 1044907.857 ms (17:24.908)
# Time: 1044956.191 ms (17:24.956)
# Time: 1045012.315 ms (17:25.012)
# Time: 1045031.871 ms (17:25.032)
# Time: 1045033.857 ms (17:25.034)
#
# Time: 1048401.504 ms (17:28.402)
# Time: 1048423.162 ms (17:28.423)
# Time: 1048486.766 ms (17:28.487)
# Time: 1048518.791 ms (17:28.519)
# Time: 1048494.556 ms (17:28.495)
#
# 3rd one again:
# Time: 1071089.965 ms (17:51.090)
# Time: 1071110.928 ms (17:51.111)
# Time: 1071146.200 ms (17:51.146)
# Time: 1071146.249 ms (17:51.146)
# Time: 1071166.552 ms (17:51.167)
#
# 3rd one again:
# Time: 1066683.682 ms (17:46.684)
# Time: 1066693.323 ms (17:46.693)
# Time: 1066730.834 ms (17:46.731)
# Time: 1066749.160 ms (17:46.749)
# Time: 1066762.901 ms (17:46.763)
#
# 3rd one again:
# Time: 1067541.838 ms (17:47.542)
# Time: 1067558.489 ms (17:47.558)
# Time: 1067565.271 ms (17:47.565)
# Time: 1067591.107 ms (17:47.591)
# Time: 1067709.423 ms (17:47.709)


# 100k with a new list of markers for each run:
# Time: 441351.914 ms (07:21.352)
# Time: 441611.074 ms (07:21.611)
# Time: 442211.427 ms (07:22.211)
# Time: 444005.728 ms (07:24.006)
# Time: 444628.682 ms (07:24.629)
#
# Time: 275451.741 ms (04:35.452)
# Time: 276651.448 ms (04:36.651)
# Time: 276869.850 ms (04:36.870)
# Time: 279133.760 ms (04:39.134)
# Time: 280027.004 ms (04:40.027)
#
# Time: 264059.263 ms (04:24.059)
# Time: 265151.931 ms (04:25.152)
# Time: 265670.066 ms (04:25.670)
# Time: 265685.427 ms (04:25.685)
# Time: 266704.066 ms (04:26.704)
#
# 3rd one again:
# Time: 49052.468 ms (00:49.052)
# Time: 49545.296 ms (00:49.545)
# Time: 50235.810 ms (00:50.236)
# Time: 52517.921 ms (00:52.518)
# Time: 53443.900 ms (00:53.444)
#
# 3rd one again:
# Time: 49002.411 ms (00:49.002)
# Time: 49555.518 ms (00:49.556)
# Time: 50743.835 ms (00:50.744)
# Time: 51794.712 ms (00:51.795)
# Time: 54155.147 ms (00:54.155)

# 3rd one again:
# Time: 48609.513 ms (00:48.610)
# Time: 49082.914 ms (00:49.083)
# Time: 52762.590 ms (00:52.763)
# Time: 53220.121 ms (00:53.220)
# Time: 54888.545 ms (00:54.889)
