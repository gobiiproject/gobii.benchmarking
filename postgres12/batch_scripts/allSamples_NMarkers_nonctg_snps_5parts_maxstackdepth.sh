#!/bin/sh

# First load the markers_input_* tables with the appropriate marker lists using ./split_marker_lists_snp.sh, then run this script.
# If desired, FIRST build new randomized marker lists using ../../randomlists/selectmarkers.

# gobii_dev=# set max_stack_depth=2000000000;
# ERROR:  invalid value for parameter "max_stack_depth": 2000000000
# DETAIL:  "max_stack_depth" must not exceed 7680kB.
# HINT:  Increase the platform's stack depth limit via "ulimit -s" or local equivalent.
# gobii_dev=# \! ulimit -s 
# unlimited

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
show max_stack_depth;
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_a mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_b mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_c mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_d mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input_e mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_e.csv' with CSV DELIMITER E'\t';" &

# 5m markers, not re-randomized:
# Time: 1074492.604 ms (17:54.493)
# Time: 1074519.148 ms (17:54.519)
# Time: 1074519.569 ms (17:54.520)
# Time: 1074522.568 ms (17:54.523)
# Time: 1074542.263 ms (17:54.542)
#
# Time: 1080929.166 ms (18:00.929)
# Time: 1080931.822 ms (18:00.932)
# Time: 1080943.784 ms (18:00.944)
# Time: 1080949.014 ms (18:00.949)
# Time: 1080984.594 ms (18:00.985)
#
# Time: 1078262.555 ms (17:58.263)
# Time: 1078287.240 ms (17:58.287)
# Time: 1078295.524 ms (17:58.296)
# Time: 1078302.251 ms (17:58.302)
# Time: 1078332.088 ms (17:58.332)
