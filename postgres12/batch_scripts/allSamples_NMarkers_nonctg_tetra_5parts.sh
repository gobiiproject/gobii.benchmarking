#!/bin/sh

# First load the markers_input_* tables with the appropriate marker lists using ./split_marker_lists.sh, then run this script.

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input_a mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_a.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input_b mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_b.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input_c mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_c.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input_d mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_d.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input_e mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allSamples_NMarkers_nonctg_e.csv' with CSV DELIMITER E'\t';" &

# 1,000 markers:
# Time: 737.454 ms
# Time: 787.570 ms
# Time: 812.905 ms
# Time: 818.096 ms
# Time: 896.997 ms
#
# Time: 680.827 ms
# Time: 694.003 ms
# Time: 708.066 ms
# Time: 734.481 ms
# Time: 784.367 ms
#
# Time: 673.175 ms
# Time: 676.740 ms
# Time: 723.997 ms
# Time: 800.685 ms
# Time: 802.298 ms


# 10,000 markers:
# Time: 5735.953 ms (00:05.736)
# Time: 5775.986 ms (00:05.776)
# Time: 5784.753 ms (00:05.785)
# Time: 5790.570 ms (00:05.791)
# Time: 6960.349 ms (00:06.960)
#
# Time: 5513.235 ms (00:05.513)
# Time: 5549.563 ms (00:05.550)
# Time: 5564.564 ms (00:05.565)
# Time: 5769.373 ms (00:05.769)
# Time: 6522.668 ms (00:06.523)
#
# Time: 5621.350 ms (00:05.621)
# Time: 5635.617 ms (00:05.636)
# Time: 5690.958 ms (00:05.691)
# Time: 5711.277 ms (00:05.711)
# Time: 6571.505 ms (00:06.572)


# 100,000 markers:
# Time: 51888.300 ms (00:51.888)
# Time: 52514.478 ms (00:52.514)
# Time: 53433.971 ms (00:53.434)
# Time: 54365.583 ms (00:54.366)
# Time: 58882.635 ms (00:58.883)
#
# Time: 52296.015 ms (00:52.296)
# Time: 52610.316 ms (00:52.610)
# Time: 52824.466 ms (00:52.824)
# Time: 53326.666 ms (00:53.327)
# Time: 59429.956 ms (00:59.430)
#
# Time: 51614.268 ms (00:51.614)
# Time: 51714.155 ms (00:51.714)
# Time: 53651.314 ms (00:53.651)
# Time: 54042.521 ms (00:54.043)
# Time: 58098.174 ms (00:58.098)
#
# Repeat 100,000 with more shared memory:
# Time: 261540.076 ms (04:21.540)
# Time: 264959.522 ms (04:24.960)
# Time: 265247.656 ms (04:25.248)
# Time: 269829.873 ms (04:29.830)
# Time: 273922.395 ms (04:33.922)
#
# Time: 52540.732 ms (00:52.541)
# Time: 52736.970 ms (00:52.737)
# Time: 53553.890 ms (00:53.554)
# Time: 53969.655 ms (00:53.970)
# Time: 57382.951 ms (00:57.383)
#
# Time: 52536.570 ms (00:52.537)
# Time: 52582.029 ms (00:52.582)
# Time: 52729.548 ms (00:52.730)
# Time: 56549.831 ms (00:56.550)
# Time: 57705.809 ms (00:57.706)


# 1,000,000 markers:
# Time: 1586687.703 ms (26:26.688)
# Time: 1591700.953 ms (26:31.701)
# Time: 1593659.484 ms (26:33.659)
# Time: 1593782.163 ms (26:33.782)
# Time: 1611237.509 ms (26:51.238)
#
# Time: 322788.525 ms (05:22.789)
# Time: 323141.067 ms (05:23.141)
# Time: 323409.948 ms (05:23.410)
# Time: 325191.648 ms (05:25.192)
# Time: 325265.496 ms (05:25.265)
#
# Time: 315972.168 ms (05:15.972)
# Time: 317603.439 ms (05:17.603)
# Time: 317875.191 ms (05:17.875)
# Time: 318296.327 ms (05:18.296)
# Time: 318892.051 ms (05:18.892)
#
# Time: 314445.265 ms (05:14.445)
# Time: 316798.334 ms (05:16.798)
# Time: 318264.369 ms (05:18.264)
# Time: 319632.725 ms (05:19.633)
# Time: 319920.836 ms (05:19.921)

# Repeat 1,000,000 with more shared memory:
# Time: 1680339.462 ms (28:00.339)
# Time: 1682781.508 ms (28:02.782)
# Time: 1683558.030 ms (28:03.558)
# Time: 1683625.084 ms (28:03.625)
# Time: 1693382.740 ms (28:13.383)
#
# Time: 314024.027 ms (05:14.024)
# Time: 318369.903 ms (05:18.370)
# Time: 320211.663 ms (05:20.212)
# Time: 321931.760 ms (05:21.932)
# Time: 323994.683 ms (05:23.995)
#
# Time: 317763.260 ms (05:17.763)
# Time: 318577.511 ms (05:18.578)
# Time: 319650.056 ms (05:19.650)
# Time: 321121.740 ms (05:21.122)
# Time: 321455.663 ms (05:21.456)


# 5,000,000 markers:
# ERROR:  could not resize shared memory segment "/PostgreSQL.172413282" to 2097152 bytes: No space left on device

# Repeat 5,000,000 with more shared memory:
# Time: 1270088.624 ms (21:10.089)
# Time: 1270098.417 ms (21:10.098)
# Time: 1270113.315 ms (21:10.113)
# Time: 1270224.943 ms (21:10.225)
# Time: 1270251.434 ms (21:10.251)
#
# Time: 1284155.952 ms (21:24.156)
# Time: 1284429.624 ms (21:24.430)
# Time: 1284430.570 ms (21:24.431)
# Time: 1284447.054 ms (21:24.447)
# Time: 1284454.234 ms (21:24.454)
#
# Time: 1291531.134 ms (21:31.531)
# Time: 1291540.084 ms (21:31.540)
# Time: 1291552.553 ms (21:31.553)
# Time: 1291672.523 ms (21:31.673)
# Time: 1291686.351 ms (21:31.686)

# 10oct20: Repeat 5m.
# Time: 1296475.960 ms (21:36.476)
# Time: 1296494.950 ms (21:36.495)
# Time: 1296507.367 ms (21:36.507)
# Time: 1296547.881 ms (21:36.548)
# Time: 1296592.742 ms (21:36.593)
#
# Time: 1294992.853 ms (21:34.993)
# Time: 1295057.898 ms (21:35.058)
# Time: 1295128.534 ms (21:35.129)
# Time: 1295246.374 ms (21:35.246)
# Time: 1295274.359 ms (21:35.274)
#
# Time: 1331642.841 ms (22:11.643)
# Time: 1331688.239 ms (22:11.688)
# Time: 1331695.181 ms (22:11.695)
# Time: 1331696.715 ms (22:11.697)
# Time: 1331713.810 ms (22:11.714)

# 1m with new list of markers for each:
# Time: 1521619.427 ms (25:21.619)
# Time: 1524921.539 ms (25:24.922)
# Time: 1527331.684 ms (25:27.332)
# Time: 1535445.390 ms (25:35.445)
# Time: 1549585.293 ms (25:49.585)
#
# Time: 1485054.606 ms (24:45.055)
# Time: 1491719.801 ms (24:51.720)
# Time: 1491875.872 ms (24:51.876)
# Time: 1492481.037 ms (24:52.481)
# Time: 1495901.337 ms (24:55.901)
#
# Time: 1506085.698 ms (25:06.086)
# Time: 1507665.270 ms (25:07.665)
# Time: 1512509.526 ms (25:12.510)
# Time: 1514828.608 ms (25:14.829)
# Time: 1532760.902 ms (25:32.761)
#
# 3rd one again:
# Time: 332332.029 ms (05:32.332)
# Time: 332330.345 ms (05:32.330)
# Time: 333251.232 ms (05:33.251)
# Time: 333299.917 ms (05:33.300)
# Time: 335420.166 ms (05:35.420)
#
# 3rd one again:
# Time: 315888.035 ms (05:15.888)
# Time: 319269.771 ms (05:19.270)
# Time: 319555.587 ms (05:19.556)
# Time: 321338.471 ms (05:21.338)
# Time: 322183.432 ms (05:22.183)
#
# 3rd one again:
# Time: 317637.944 ms (05:17.638)
# Time: 318160.223 ms (05:18.160)
# Time: 318452.093 ms (05:18.452)
# Time: 321503.271 ms (05:21.503)
# Time: 325504.721 ms (05:25.505)

# 5m with new list of markers for each:
# Time: 1227334.396 ms (20:27.334)
# Time: 1227353.185 ms (20:27.353)
# Time: 1227355.518 ms (20:27.356)
# Time: 1227446.779 ms (20:27.447)
# Time: 1227453.187 ms (20:27.453)
#
# Time: 1242024.735 ms (20:42.025)
# Time: 1242085.812 ms (20:42.086)
# Time: 1242145.830 ms (20:42.146)
# Time: 1242206.262 ms (20:42.206)
# Time: 1242236.282 ms (20:42.236)
#
# Time: 1248218.940 ms (20:48.219)
# Time: 1248224.385 ms (20:48.224)
# Time: 1248227.415 ms (20:48.227)
# Time: 1248248.513 ms (20:48.249)
# Time: 1248251.520 ms (20:48.252)
#
# 3rd one again:
# Time: 1277580.819 ms (21:17.581)
# Time: 1277584.741 ms (21:17.585)
# Time: 1277584.832 ms (21:17.585)
# Time: 1277602.676 ms (21:17.603)
# Time: 1277611.482 ms (21:17.611)
#
# 3rd one again:
# Time: 1285724.647 ms (21:25.725)
# Time: 1285726.701 ms (21:25.727)
# Time: 1285754.969 ms (21:25.755)
# Time: 1285763.546 ms (21:25.764)
# Time: 1285782.024 ms (21:25.782)
#
# 3rd one again:
# Time: 1282133.785 ms (21:22.134)
# Time: 1282165.498 ms (21:22.165)
# Time: 1282172.651 ms (21:22.173)
# Time: 1282198.449 ms (21:22.198)
# Time: 1282249.560 ms (21:22.250)

# 100k with new list of markers for each:
# Time: 443349.642 ms (07:23.350)
# Time: 443697.246 ms (07:23.697)
# Time: 445245.614 ms (07:25.246)
# Time: 448980.775 ms (07:28.981)
# Time: 452276.587 ms (07:32.277)
#
# Time: 293224.454 ms (04:53.224)
# Time: 294748.728 ms (04:54.749)
# Time: 295278.427 ms (04:55.278)
# Time: 296671.392 ms (04:56.671)
# Time: 297805.628 ms (04:57.806)
#
# Time: 282608.738 ms (04:42.609)
# Time: 283786.219 ms (04:43.786)
# Time: 284109.037 ms (04:44.109)
# Time: 284162.521 ms (04:44.163)
# Time: 285247.279 ms (04:45.247)
#
# 3rd one again:
# Time: 52758.823 ms (00:52.759)
# Time: 53014.463 ms (00:53.014)
# Time: 54564.353 ms (00:54.564)
# Time: 54832.746 ms (00:54.833)
# Time: 58985.079 ms (00:58.985)
#
# 3rd one again:
# Time: 52002.135 ms (00:52.002)
# Time: 52176.418 ms (00:52.176)
# Time: 53184.945 ms (00:53.185)
# Time: 54205.004 ms (00:54.205)
# Time: 58446.803 ms (00:58.447)
#
# 3rd one again:
# Time: 52142.114 ms (00:52.142)
# Time: 52786.975 ms (00:52.787)
# Time: 53109.659 ms (00:53.110)
# Time: 55506.196 ms (00:55.506)
# Time: 59462.733 ms (00:59.463)

########
# 22dec20: Re-run in descending order of size: 5m, 1m, 100k.  Use a different random list for each replicate.

# 5m markers:
# Time: 1296054.357 ms (21:36.054)
# Time: 1296060.025 ms (21:36.060)
# Time: 1296059.484 ms (21:36.059)
# Time: 1296089.158 ms (21:36.089)
# Time: 1296161.124 ms (21:36.161)
#
# Time: 1292740.896 ms (21:32.741)
# Time: 1292793.218 ms (21:32.793)
# Time: 1292992.712 ms (21:32.993)
# Time: 1293008.849 ms (21:33.009)
# Time: 1293024.223 ms (21:33.024)
#
# Time: 1300335.964 ms (21:40.336)
# Time: 1300354.749 ms (21:40.355)
# Time: 1300382.732 ms (21:40.383)
# Time: 1300401.551 ms (21:40.402)
# Time: 1300498.625 ms (21:40.499)

# 1m markers:
# Time: 1586375.502 ms (26:26.376)
# Time: 1590908.634 ms (26:30.909)
# Time: 1592049.041 ms (26:32.049)
# Time: 1593310.281 ms (26:33.310)
# Time: 1607932.881 ms (26:47.933)
#
# Time: 1547312.161 ms (25:47.312)
# Time: 1547945.204 ms (25:47.945)
# Time: 1548946.388 ms (25:48.946)
# Time: 1550699.814 ms (25:50.700)
# Time: 1582065.687 ms (26:22.066)
#
# Time: 1526879.527 ms (25:26.880)
# Time: 1529500.775 ms (25:29.501)
# Time: 1530058.303 ms (25:30.058)
# Time: 1546388.736 ms (25:46.389)
# Time: 1555060.436 ms (25:55.060)

# 100k markers:
# Time: 265111.660 ms (04:25.112)
# Time: 266206.664 ms (04:26.207)
# Time: 266259.974 ms (04:26.260)
# Time: 268057.125 ms (04:28.057)
# Time: 270443.772 ms (04:30.444)
#
# Time: 253576.810 ms (04:13.577)
# Time: 253617.190 ms (04:13.617)
# Time: 254810.232 ms (04:14.810)
# Time: 258183.340 ms (04:18.183)
# Time: 258267.650 ms (04:18.268)
#
# Time: 254483.349 ms (04:14.483)
# Time: 254688.683 ms (04:14.689)
# Time: 255583.742 ms (04:15.584)
# Time: 256291.744 ms (04:16.292)
# Time: 258027.684 ms (04:18.028)

