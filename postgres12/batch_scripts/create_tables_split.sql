drop table markers_input_a;
CREATE TABLE public.markers_input_a
(
    id SERIAL PRIMARY KEY,
    marker_id text
)
WITH (
    OIDS = FALSE
     )
TABLESPACE pg_default;
	    
drop table markers_input_b;
CREATE TABLE public.markers_input_b
(
    id SERIAL PRIMARY KEY,
    marker_id text
)
WITH (
    OIDS = FALSE
     )
TABLESPACE pg_default;
	    
drop table markers_input_c;
CREATE TABLE public.markers_input_c
(
    id SERIAL PRIMARY KEY,
    marker_id text
)
WITH (
    OIDS = FALSE
     )
TABLESPACE pg_default;
	    
drop table markers_input_d;
CREATE TABLE public.markers_input_d
(
    id SERIAL PRIMARY KEY,
    marker_id text
)
WITH (
    OIDS = FALSE
     )
TABLESPACE pg_default;
	    
drop table markers_input_e;
CREATE TABLE public.markers_input_e
(
    id SERIAL PRIMARY KEY,
    marker_id text
)
WITH (
    OIDS = FALSE
     )
TABLESPACE pg_default;
	    
