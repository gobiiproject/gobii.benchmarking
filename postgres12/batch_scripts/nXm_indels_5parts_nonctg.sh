#!/bin/sh
###### M markers x N samples non-contiguous #########
# Divide the table into five partitions.

# # Before running this script,
# # 1. Load the markers_input_* tables with the lists of markers to be queried, using ./split_marker_lists_snp.sh.
# # 2. Load table samples_input with the list of samples to be queried, as follows:
# -- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
# -- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
# truncate samples_input;
# copy public.samples_input (sample_id)
# FROM PROGRAM 'cat /data/gobii.benchmarking/randomlists/SNPs_only/pg_samples.3000.5 | tr "\n" "\t"'
# DELIMITER E',';

#1
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, markers_input_a mi, samples_input si
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_A.csv' with CSV DELIMITER E'\t';" &

#2
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, markers_input_b mi, samples_input si
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_B.csv' with CSV DELIMITER E'\t';" &

#3
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, markers_input_c mi, samples_input si
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_C.csv' with CSV DELIMITER E'\t';" &

#4
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, markers_input_d mi, samples_input si
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_D.csv' with CSV DELIMITER E'\t';" &

#5
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
set max_stack_depth=7680;
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_indel mgs, markers_input_e mi, samples_input si
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_E.csv' with CSV DELIMITER E'\t';" &

### 1000 samples, 50,000 markers:
# Time: 206119.223 ms (03:26.119)
# Time: 206355.021 ms (03:26.355)
# Time: 206498.966 ms (03:26.499)
# Time: 207057.538 ms (03:27.058)
# Time: 212006.538 ms (03:32.007)
#
# Time: 13298.259 ms (00:13.298)
# Time: 13420.753 ms (00:13.421)
# Time: 13509.283 ms (00:13.509)
# Time: 14200.674 ms (00:14.201)
# Time: 15015.159 ms (00:15.015)
#
# Time: 13395.487 ms (00:13.395)
# Time: 13423.002 ms (00:13.423)
# Time: 13480.126 ms (00:13.480)
# Time: 13870.692 ms (00:13.871)
# Time: 15618.134 ms (00:15.618)
#
# Time: 13442.131 ms (00:13.442)
# Time: 13945.952 ms (00:13.946)
# Time: 14227.148 ms (00:14.227)
# Time: 14266.208 ms (00:14.266)
# Time: 14459.204 ms (00:14.459)
#
# Time: 13708.255 ms (00:13.708)
# Time: 13797.454 ms (00:13.797)
# Time: 13801.850 ms (00:13.802)
# Time: 13916.072 ms (00:13.916)
# Time: 17607.190 ms (00:17.607)
#
# New marker lists:
# Time: 138939.071 ms (02:18.939)
# Time: 139073.397 ms (02:19.073)
# Time: 140616.701 ms (02:20.617)
# Time: 140762.762 ms (02:20.763)
# Time: 142205.522 ms (02:22.206)
#
# New marker and sample lists:
# Time: 122107.362 ms (02:02.107)
# Time: 122804.309 ms (02:02.804)
# Time: 123240.495 ms (02:03.240)
# Time: 123259.166 ms (02:03.259)
# Time: 124069.254 ms (02:04.069)
#
# New marker and sample lists:
# Time: 119115.732 ms (01:59.116)
# Time: 119569.335 ms (01:59.569)
# Time: 119619.395 ms (01:59.619)
# Time: 119679.209 ms (01:59.679)
# Time: 121211.439 ms (02:01.211)

### 2000 samples, 50,000 markers:
# Time: 103123.543 ms (01:43.124)
# Time: 104450.209 ms (01:44.450)
# Time: 104873.512 ms (01:44.874)
# Time: 106038.985 ms (01:46.039)
# Time: 106124.431 ms (01:46.124)
#
# New marker and sample lists:
# Time: 100172.704 ms (01:40.173)
# Time: 101481.734 ms (01:41.482)
# Time: 102589.634 ms (01:42.590)
# Time: 103501.471 ms (01:43.501)
# Time: 106650.887 ms (01:46.651)
#
# New marker and sample lists:
# Time: 98062.498 ms (01:38.062)
# Time: 98155.775 ms (01:38.156)
# Time: 99785.351 ms (01:39.785)
# Time: 99951.710 ms (01:39.952)
# Time: 101350.836 ms (01:41.351)
#
# Old lists:
# Time: 16756.528 ms (00:16.757)
# Time: 16774.073 ms (00:16.774)
# Time: 17149.363 ms (00:17.149)
# Time: 18157.827 ms (00:18.158)
# Time: 18362.125 ms (00:18.362)
#
# Time: 16940.062 ms (00:16.940)
# Time: 16992.869 ms (00:16.993)
# Time: 17075.309 ms (00:17.075)
# Time: 18359.659 ms (00:18.360)
# Time: 18436.249 ms (00:18.436)
#
# Time: 16831.029 ms (00:16.831)
# Time: 16884.731 ms (00:16.885)
# Time: 16930.296 ms (00:16.930)
# Time: 17715.888 ms (00:17.716)
# Time: 19521.530 ms (00:19.522)


### 3000 samples, 50,000 markers:
# Time: 19941.433 ms (00:19.941)
# Time: 20040.981 ms (00:20.041)
# Time: 20858.216 ms (00:20.858)
# Time: 21419.502 ms (00:21.420)
# Time: 24870.010 ms (00:24.870)
#
# New sample and marker lists:
# Time: 19919.987 ms (00:19.920)
# Time: 19966.811 ms (00:19.967)
# Time: 20385.516 ms (00:20.386)
# Time: 21081.583 ms (00:21.082)
# Time: 23792.112 ms (00:23.792)
#
# New sample and marker lists:
# Time: 19863.148 ms (00:19.863)
# Time: 19941.860 ms (00:19.942)
# Time: 20525.867 ms (00:20.526)
# Time: 21305.282 ms (00:21.305)
# Time: 23834.433 ms (00:23.834)
#
# Old lists:
# Time: 20206.012 ms (00:20.206)
# Time: 20278.597 ms (00:20.279)
# Time: 21207.741 ms (00:21.208)
# Time: 21574.842 ms (00:21.575)
# Time: 22622.880 ms (00:22.623)
#
# Time: 20001.609 ms (00:20.002)
# Time: 20068.515 ms (00:20.069)
# Time: 20481.080 ms (00:20.481)
# Time: 21382.361 ms (00:21.382)
# Time: 22681.381 ms (00:22.681)
#
# Virgin lists, never used for any test:
# Time: 99718.656 ms (01:39.719)
# Time: 99907.832 ms (01:39.908)
# Time: 101433.637 ms (01:41.434)
# Time: 102514.745 ms (01:42.515)
# Time: 103114.683 ms (01:43.115)
#
# Time: 99493.561 ms (01:39.494)
# Time: 100132.143 ms (01:40.132)
# Time: 101445.579 ms (01:41.446)
# Time: 102942.041 ms (01:42.942)
# Time: 106099.787 ms (01:46.100)
#
# Time: 99408.542 ms (01:39.409)
# Time: 99533.185 ms (01:39.533)
# Time: 100236.680 ms (01:40.237)
# Time: 101150.227 ms (01:41.150)
# Time: 102133.441 ms (01:42.133)
#
# Same as last lists:
# Time: 19806.223 ms (00:19.806)
# Time: 20066.838 ms (00:20.067)
# Time: 20428.367 ms (00:20.428)
# Time: 21761.022 ms (00:21.761)
# Time: 23652.109 ms (00:23.652)
#
# Time: 20253.382 ms (00:20.253)
# Time: 20319.802 ms (00:20.320)
# Time: 20502.910 ms (00:20.503)
# Time: 20529.885 ms (00:20.530)
# Time: 24213.401 ms (00:24.213)
#
# Time: 20284.445 ms (00:20.284)
# Time: 20365.544 ms (00:20.366)
# Time: 20380.710 ms (00:20.381)
# Time: 20743.847 ms (00:20.744)
# Time: 22977.603 ms (00:22.978)


### 1000 samples, 1,000,000 markers:
# Time: 1025120.520 ms (17:05.121)
# Time: 1025638.668 ms (17:05.639)
# Time: 1028159.407 ms (17:08.159)
# Time: 1028944.931 ms (17:08.945)
# Time: 1030932.987 ms (17:10.933)
#
# New sample and marker lists:
# Time: 1006992.483 ms (16:46.992)
# Time: 1019545.897 ms (16:59.546)
# Time: 1025461.095 ms (17:05.461)
# Time: 1026228.948 ms (17:06.229)
# Time: 1030946.728 ms (17:10.947)
#
# New sample and marker lists:
# Time: 988140.825 ms (16:28.141)
# Time: 994075.323 ms (16:34.075)
# Time: 996447.083 ms (16:36.447)
# Time: 1007158.947 ms (16:47.159)
# Time: 1009533.009 ms (16:49.533)
#
# Old lists:
# Time: 167398.657 ms (02:47.399)
# Time: 167752.739 ms (02:47.753)
# Time: 167920.902 ms (02:47.921)
# Time: 168574.083 ms (02:48.574)
# Time: 169637.630 ms (02:49.638)
#
# Time: 141294.627 ms (02:21.295)
# Time: 141793.203 ms (02:21.793)
# Time: 142530.228 ms (02:22.530)
# Time: 142743.564 ms (02:22.744)
# Time: 142871.744 ms (02:22.872)
#
# Time: 142473.301 ms (02:22.473)
# Time: 143843.083 ms (02:23.843)
# Time: 144350.245 ms (02:24.350)
# Time: 144951.267 ms (02:24.951)
# Time: 145996.481 ms (02:25.996)

### 2000 samples, 1,000,000 markers:
# Time: 1012729.915 ms (16:52.730)
# Time: 1034710.428 ms (17:14.710)
# Time: 1044687.113 ms (17:24.687)
# Time: 1049029.749 ms (17:29.030)
# Time: 1050054.510 ms (17:30.055)
#
# New marker and sample lists:
# Time: 1038496.848 ms (17:18.497)
# Time: 1048786.054 ms (17:28.786)
# Time: 1055212.325 ms (17:35.212)
# Time: 1056748.555 ms (17:36.749)
# Time: 1070974.108 ms (17:50.974) + 27 s to catenate the /data/polyploid_indel_benchmarking/pg12_output/snp_dataset/mMarkers_nSamples_* files
#
# New marker and sample lists:
# Time: 1035267.285 ms (17:15.267)
# Time: 1060162.499 ms (17:40.162)
# Time: 1069426.008 ms (17:49.426)
# Time: 1074583.445 ms (17:54.583)
# Time: 1083983.963 ms (18:03.984) + 29 s
#
# Old lists:
# Time: 213420.838 ms (03:33.421)
# Time: 215312.401 ms (03:35.312)
# Time: 215551.241 ms (03:35.551)
# Time: 215646.620 ms (03:35.647)
# Time: 216501.716 ms (03:36.502) + 31 s
#
# Time: 183647.170 ms (03:03.647)
# Time: 185624.019 ms (03:05.624)
# Time: 186926.828 ms (03:06.927)
# Time: 187167.702 ms (03:07.168)
# Time: 187390.736 ms (03:07.391) + 32 s
#
# Time: 183362.565 ms (03:03.363)
# Time: 183463.146 ms (03:03.463)
# Time: 185431.457 ms (03:05.431)
# Time: 186346.400 ms (03:06.346)
# Time: 186677.675 ms (03:06.678) + 30 s


### 3000 samples, 1,000,000 markers:
# Time: 1345501.782 ms (22:25.502)
# Time: 1356478.895 ms (22:36.479)
# Time: 1357192.125 ms (22:37.192)
# Time: 1359029.992 ms (22:39.030)
# Time: 1359527.716 ms (22:39.528)
#
# New marker and sample lists:
# Time: 1213849.671 ms (20:13.850)
# Time: 1214081.188 ms (20:14.081)
# Time: 1215723.843 ms (20:15.724)
# Time: 1219133.869 ms (20:19.134)
# Time: 1226146.599 ms (20:26.147)
#
# New marker and sample lists:
# Time: 1208829.955 ms (20:08.830)
# Time: 1212538.892 ms (20:12.539)
# Time: 1213122.516 ms (20:13.123)
# Time: 1218121.598 ms (20:18.122)
# Time: 1222814.324 ms (20:22.814)
#
# Old lists:
# Time: 225007.235 ms (03:45.007)
# Time: 225049.733 ms (03:45.050)
# Time: 225780.839 ms (03:45.781)
# Time: 226090.057 ms (03:46.090)
# Time: 227809.170 ms (03:47.809)
#
# Time: 223965.296 ms (03:43.965)
# Time: 224105.247 ms (03:44.105)
# Time: 225378.454 ms (03:45.378)
# Time: 225568.753 ms (03:45.569)
# Time: 225588.949 ms (03:45.589)
#
# Time: 224092.232 ms (03:44.092)
# Time: 226116.471 ms (03:46.116)
# Time: 226139.120 ms (03:46.139)
# Time: 227282.299 ms (03:47.282)
# Time: 228249.044 ms (03:48.249)
