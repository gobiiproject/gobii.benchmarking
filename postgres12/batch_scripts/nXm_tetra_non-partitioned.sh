###### CONTIGUOUS #######

## 1k x 1k
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 1100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1kMarkers_1kSamples.csv' with CSV DELIMITER E'\t';
";

## 50k x 1k
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 1000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 50100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/50kMarkers_1kSamples.csv' with CSV DELIMITER E'\t';
";

## 50k x 3k
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "\timing" -c "
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 50100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/50kMarkers_3kSamples.csv' with CSV DELIMITER E'\t';
";

