#!/bin/sh

# # For SNPs+Indels, Diploid
# # Split each list of markernames into 5 pieces.
# # Load them into separate Postgres tables.

# cd /data/gobii.benchmarking/randomlists/Indels
# split -a 1 -l 20000 pg_markers_indel.100k.1 split/pg_markers_indel.100k.1.
# split -a 1 -l 20000 pg_markers_indel.100k.2 split/pg_markers_indel.100k.2.
# split -a 1 -l 20000 pg_markers_indel.100k.3 split/pg_markers_indel.100k.3.
# split -a 1 -l 200000 pg_markers_indel.1m.1 split/pg_markers_indel.1m.1.
# split -a 1 -l 200000 pg_markers_indel.1m.2 split/pg_markers_indel.1m.2.
# split -a 1 -l 200000 pg_markers_indel.1m.3 split/pg_markers_indel.1m.3.
# split -a 1 -l 1000000 pg_markers_indel.5m.1 split/pg_markers_indel.5m.1.
# split -a 1 -l 1000000 pg_markers_indel.5m.2 split/pg_markers_indel.5m.2.
# split -a 1 -l 1000000 pg_markers_indel.5m.3 split/pg_markers_indel.5m.3.
# split -a 1 -l 10000 pg_markers_indel.50k split/pg_markers_indel.50k.
# split -a 1 -l 10000 pg_markers_indel.50k.1 split/pg_markers_indel.50k.1.
# split -a 1 -l 10000 pg_markers_indel.50k.2 split/pg_markers_indel.50k.2.
# split -a 1 -l 10000 pg_markers_indel.50k.3 split/pg_markers_indel.50k.3.
# split -a 1 -l 10000 pg_markers_indel.50k.4 split/pg_markers_indel.50k.4.
# split -a 1 -l 10000 pg_markers_indel.50k.5 split/pg_markers_indel.50k.5.
# split -a 1 -l 10000 pg_markers_indel.50k.6 split/pg_markers_indel.50k.6.
# split -a 1 -l 10000 pg_markers_indel.50k.7 split/pg_markers_indel.50k.7.
# split -a 1 -l 10000 pg_markers_indel.50k.8 split/pg_markers_indel.50k.8.
# split -a 1 -l 10000 pg_markers_indel.50k.9 split/pg_markers_indel.50k.9.


# Load each list into five split Postgres tables.
# 50k lists:
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_a;
copy public.markers_input_a (marker_id)
from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.7.a';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_b;
copy public.markers_input_b (marker_id)
from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.7.b';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_c;
copy public.markers_input_c (marker_id)
from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.7.c';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_d;
copy public.markers_input_d (marker_id)
from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.7.d';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_e;
copy public.markers_input_e (marker_id)
from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.7.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.5.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.5.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.5.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.5.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.5.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.6.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.6.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.6.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.6.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.50k.6.e';"


# 100k lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.1.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.2.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.3.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.3.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.3.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.3.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.100k.3.e';"

# # 1m lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.1.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.2.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.3.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.3.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.3.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.3.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.1m.3.e';"


# # 5m lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.1.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.2.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.3.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.3.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.3.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.3.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Indels/split/pg_markers_indel.5m.3.e';"

