#!/bin/sh

# Split each list of markernames into 5 pieces.

## For SNPs only:
# cd /data/polyploid_indel_benchmarking/randomlists
# No,
# cd /data/gobii.benchmarking/randomlists/SNPs_only

# split -a 1 -l 200000 pg_markers.1m.1 split/pg_markers.1m.1.
# split -a 1 -l 200000 pg_markers.1m.2 split/pg_markers.1m.2.
# split -a 1 -l 200000 pg_markers.1m.3 split/pg_markers.1m.3.
# split -a 1 -l 1000000 pg_markers.5m.1 split/pg_markers.5m.1.
# split -a 1 -l 1000000 pg_markers.5m.2 split/pg_markers.5m.2.
# split -a 1 -l 1000000 pg_markers.5m.3 split/pg_markers.5m.3.
# split -a 1 -l 20000 pg_markers.100k.1 split/pg_markers.100k.1.
# split -a 1 -l 20000 pg_markers.100k.2 split/pg_markers.100k.2.
# split -a 1 -l 20000 pg_markers.100k.3 split/pg_markers.100k.3.

# # Load each list partition into a separate Postgres table.
PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_a;
copy public.markers_input_a (marker_id)
from '/data/gobii.benchmarking/randomlists/SNPs_only/split/pg_markers.5m.3.a';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_b;
copy public.markers_input_b (marker_id)
from '/data/gobii.benchmarking/randomlists/SNPs_only/split/pg_markers.5m.3.b';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_c;
copy public.markers_input_c (marker_id)
from '/data/gobii.benchmarking/randomlists/SNPs_only/split/pg_markers.5m.3.c';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_d;
copy public.markers_input_d (marker_id)
from '/data/gobii.benchmarking/randomlists/SNPs_only/split/pg_markers.5m.3.d';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_e;
copy public.markers_input_e (marker_id)
from '/data/gobii.benchmarking/randomlists/SNPs_only/split/pg_markers.5m.3.e';"
