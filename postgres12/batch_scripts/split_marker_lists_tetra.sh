#!/bin/sh

# # For SNPs+Indels+Tetraploids
# # Split each list of markernames into 5 pieces.
# # Load them into separate Postgres tables.
# # First, prepare the random lists with /data/gobii.benchmarking/randomlists/selectmarkers

# cd /data/gobii.benchmarking/randomlists/Tetraploid
# split -a 1 -l 200000 pg_markers_tetraploid.1m.1 split/pg_markers_tetraploid.1m.1.
# split -a 1 -l 200000 pg_markers_tetraploid.1m.2 split/pg_markers_tetraploid.1m.2.
# split -a 1 -l 200000 pg_markers_tetraploid.1m.3 split/pg_markers_tetraploid.1m.3.
# split -a 1 -l 1000000 pg_markers_tetraploid.5m.1 split/pg_markers_tetraploid.5m.1.
# split -a 1 -l 1000000 pg_markers_tetraploid.5m.2 split/pg_markers_tetraploid.5m.2.
# split -a 1 -l 1000000 pg_markers_tetraploid.5m.3 split/pg_markers_tetraploid.5m.3.
# split -a 1 -l 20000 pg_markers_tetraploid.100k.1 split/pg_markers_tetraploid.100k.1.
# split -a 1 -l 20000 pg_markers_tetraploid.100k.2 split/pg_markers_tetraploid.100k.2.
# split -a 1 -l 20000 pg_markers_tetraploid.100k.3 split/pg_markers_tetraploid.100k.3.

# # Load each list into one of five Postgres tables.
# # 5m lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.1.e';"


# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.2.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.3.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.3.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.3.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.3.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.5m.3.e';"


# # 1m lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.1.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.2.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.3.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.3.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.3.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.3.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.1m.3.e';"


# 100k lists:
# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.1.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.1.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.1.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.1.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.1.e';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_a;
# copy public.markers_input_a (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.2.a';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_b;
# copy public.markers_input_b (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.2.b';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_c;
# copy public.markers_input_c (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.2.c';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_d;
# copy public.markers_input_d (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.2.d';"

# PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
# truncate markers_input_e;
# copy public.markers_input_e (marker_id)
# from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.2.e';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_a;
copy public.markers_input_a (marker_id)
from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.3.a';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_b;
copy public.markers_input_b (marker_id)
from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.3.b';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_c;
copy public.markers_input_c (marker_id)
from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.3.c';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_d;
copy public.markers_input_d (marker_id)
from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.3.d';"

PGPASSWORD=g0b11isw3s0m3 psql -h localhost -U appuser -d gobii_dev -c "
truncate markers_input_e;
copy public.markers_input_e (marker_id)
from '/data/gobii.benchmarking/randomlists/Tetraploid/split/pg_markers_tetraploid.100k.3.e';"

