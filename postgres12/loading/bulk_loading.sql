
###BULK LOADING
Load:

copy public.marker_genotype_snp (marker_genotype_id, dataset_id, chrom, pos, marker_name, genotype)
FROM '/data/polyploid_indel_benchmarking/loading_postgres_dem/generated_dataset_snp_parsed.out' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

copy public.marker_genotype_indel (marker_genotype_id, dataset_id, chrom, pos, marker_name, genotype) FROM '/data/polyploid_indel_benchmarking/loading_postgres_dem/generated_dataset_indels100_parsed.out' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

copy public.marker_genotype_tetraploid (marker_genotype_id, dataset_id, chrom, pos, marker_name, genotype) FROM '/data/polyploid_indel_benchmarking/loading_postgres_dem/generated_dataset_tetraploid_parsed.out' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';



#### MARKER LISTS
drop table markers_input;
CREATE TABLE public.markers_input
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


drop table tmp_markers;
CREATE TABLE public.tmp_markers
(
    marker_id text
);

copy public.markers_input (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.1000' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

copy public.markers_input (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.10k' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.10k' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.100k' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';



-----------------
Time: 782.935 ms
copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.1m' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';

COPY 4999999
Time: 3373.751 ms (00:03.374)
copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.5m' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';


INSERT INTO markers_input
SELECT *
FROM tmp_markers
ON CONFLICT DO NOTHING;

INSERT INTO markers_input (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

CREATE TABLE public.markers_input_5m
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--INSERT 0 4419599
--Time: 96016.330 ms (01:36.016)
INSERT INTO markers_input_5m (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

---------- INDELS INPUT ----------

copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/Indels/pg_markers_indel.1000' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';


CREATE TABLE markers_input_indels_1k
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
);

INSERT INTO markers_input_indels_1k (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

---
copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/Indels/pg_markers_indel.10k' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';
CREATE TABLE markers_input_indels_10k
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

INSERT INTO markers_input_indels_10k (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

---
copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/Indels/pg_markers_indel.100k' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';
CREATE TABLE markers_input_indels_100k
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

INSERT INTO markers_input_indels_100k (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

---
copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/Indels/pg_markers_indel.1m' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';
CREATE TABLE markers_input_indels_1m
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


INSERT INTO markers_input_indels_1m (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;

--

copy public.tmp_markers (marker_id) FROM '/data/polyploid_indel_benchmarking/randomlists/Indels/pg_markers_indel.5m' DELIMITER E'\t' CSV HEADER QUOTE E'\x01' ESCAPE '^';
CREATE TABLE markers_input_indels_5m
(
    id SERIAL PRIMARY KEY,
    marker_id text UNIQUE NOT NULL
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


INSERT INTO markers_input_indels_5m (marker_id)
SELECT marker_id
FROM tmp_markers
ON CONFLICT DO NOTHING;


### SAMPLE LISTS:

drop table samples_input;
CREATE TABLE public.samples__input
(
    id SERIAL PRIMARY KEY,
    sample_id text UNIQUE NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
-- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
truncate samples_input;
copy public.samples_input (sample_id)
FROM PROGRAM 'cat /data/polyploid_indel_benchmarking/randomlists/pg_samples.100 | tr "\n" "\t"'
DELIMITER E',';
