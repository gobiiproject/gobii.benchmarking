CREATE TABLE public.marker_genotype_snp
(
    marker_genotype_id integer NOT NULL,
    dataset_id integer NOT NULL,
    chrom text NOT NULL,
    pos integer NOT NULL,
    marker_name text NOT NULL,
    genotype jsonb NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


CREATE TABLE public.marker_genotype_indel
(
    marker_genotype_id integer NOT NULL,
    dataset_id integer NOT NULL,
    chrom text NOT NULL,
    pos integer NOT NULL,
    marker_name text NOT NULL,
    genotype jsonb NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--not being used - non-transposed version was faster
CREATE TABLE sample_genotype_snp
(
    sample_genotyp_id integer NOT NULL,
    sample_id text NOT NULL,
    genotype jsonb NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.marker_genotype_tetraploid
(
    marker_genotype_id integer NOT NULL,
    dataset_id integer NOT NULL,
    chrom text NOT NULL,
    pos integer NOT NULL,
    marker_name text NOT NULL,
    genotype jsonb NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;