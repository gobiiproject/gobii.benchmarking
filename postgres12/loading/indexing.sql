CREATE INDEX genotype_idx ON marker_genotype_snp USING gin(genotype);
Time: 60678471.962 ms (16:51:18.472)


CREATE INDEX genotype_idx_btree ON marker_genotype_snp (genotype);
--22s
CREATE INDEX chrom_idx_btree ON marker_genotype_snp (chrom);
--12s
CREATE INDEX pos_idx_btree ON marker_genotype_snp (pos);
--28s
CREATE INDEX chrom_pos_idx_btree ON marker_genotype_snp (chrom, pos);
CREATE INDEX marker_name_idx_btree ON marker_genotype_snp (marker_name);

-------------- INDELS
ALTER TABLE marker_genotype_indel ADD PRIMARY KEY (marker_genotype_id);

CREATE INDEX indel_chrom_idx_btree ON marker_genotype_indel (chrom);
CREATE INDEX indel_pos_idx_btree ON marker_genotype_indel (pos);
CREATE INDEX indel_chrom_pos_idx_btree ON marker_genotype_indel (chrom, pos);
CREATE INDEX indel_marker_name_idx_btree ON marker_genotype_indel (marker_name);

CREATE INDEX indel_genotype_idx ON marker_genotype_indel USING gin(genotype);

CREATE INDEX indel_genotype_idx_btree ON marker_genotype_indel (genotype); --ERROR:  index row requires 13040 bytes, maximum size is 8191

--Sample duration
--gobii_dev=# ALTER TABLE marker_genotype_indel ADD PRIMARY KEY (marker_genotype_id);
--ALTER TABLE
--Time: 9336.952 ms (00:09.337)
--gobii_dev=# CREATE INDEX indel_chrom_idx_btree ON marker_genotype_indel (chrom);
--CREATE INDEX
--Time: 18411.523 ms (00:18.412)
--gobii_dev=# CREATE INDEX indel_pos_idx_btree ON marker_genotype_indel (pos);
--CREATE INDEX
--Time: 10600.177 ms (00:10.600)
--gobii_dev=# CREATE INDEX indel_chrom_pos_idx_btree ON marker_genotype_indel (chrom, pos);
--CREATE INDEX
T--ime: 28551.773 ms (00:28.552)

-------------- TETRAPLOID
ALTER TABLE marker_genotype_tetraploid ADD PRIMARY KEY (marker_genotype_id);


CREATE INDEX tetra_chrom_idx_btree ON marker_genotype_tetraploid (chrom);
CREATE INDEX tetra_pos_idx_btree ON marker_genotype_tetraploid (pos);
CREATE INDEX tetra_chrom_pos_idx_btree ON marker_genotype_tetraploid (chrom, pos);
CREATE INDEX tetra_marker_name_idx_btree ON marker_genotype_tetraploid (marker_name);


CREATE INDEX tetra_genotype_idx ON marker_genotype_indel USING gin(genotype);


CREATE INDEX indel_genotype_idx_btree ON marker_genotype_indel (genotype); --ERROR:  index row requires 13040 bytes, maximum size is 8191


--**1am NOTE TO SELF: When you start trying to do col->key in dynamic select statements, make sure to concat them as one big string and not individual columns as doing the latter will probably hit the 250-1k+ column number limit!