-- ####### ALL Samples X N Markers ########

-- #CONTIG

-- ##>MOVED to parallel jobs
-- #5,000,000 markers

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 5000100) to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/5mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 5000100) to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/5mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs
where marker_genotype_id < 5000100) to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/5mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

-- Kevin:
-- Time: 2560869.186 ms (42:40.869)

-- Dave:
-- Time: 2547557.266 ms (42:27.557)
-- Time: 2567071.183 ms (42:47.071)
-- Time: 2560620.639 ms (42:40.621)
