genotype = {}
genotype['variant1'] = 'C/T'
genotype['variant2'] = 'A/G'

sample_ids = ('variant1', 'variant2')

samplecount = 2

def parse_sample_list(genotype, sample_ids, samplecount):
    parsed = []
    for x in range (0, samplecount):
        parsed.append(genotype[sample_ids[x]])
    return '\t'.join(parsed)

print(parse_sample_list(genotype, sample_ids, samplecount))

# {} set or dict, [] list, () function

