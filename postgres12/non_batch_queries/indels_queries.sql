#NON-CONTIG

#1000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_1k mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1kMarkers_allSamples_nonctg.csv' with CSV DELIMITER E'\t';

#10k markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_10k mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/10kMarkers_allSamples_nonctg_a.csv' with CSV DELIMITER E'\t';

#100k markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
inner join markers_input_indels_100k mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/100kMarkers_allSamples_nonctg_a.csv' with CSV DELIMITER E'\t';


##########################################################################################
#CONTIG

#1000 markers ctg
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_indel mgs
where marker_genotype_id < 1100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#10,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 10100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/10kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#100,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 100100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/100kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

##>MOVED to parallel jobs
#1,000,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 1000100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/1mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

##>MOVED to parallel jobs
#5,000,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 5000100) to '/data/polyploid_indel_benchmarking/pg12_output/indel_dataset/5mMarkers_allSamples.csv' with CSV DELIMITER E'\t';
