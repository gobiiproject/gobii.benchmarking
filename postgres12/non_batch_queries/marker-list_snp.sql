-- All samples for a list of markers:

-- Create the table to hold the queried marker names.
-- Don't make marker_id column UNIQUE, since the random markers might include a few duplications.
--
-- drop table markers_input;
-- CREATE TABLE public.markers_input
-- (
--     id SERIAL PRIMARY KEY,
--     marker_id text 
-- )
-- WITH (
--     OIDS = FALSE
-- )
-- TABLESPACE pg_default;

-- The table from Kevin was:
--
-- gobii_dev=# \d markers_input;
-- Table "public.markers_input"
-- Column   |  Type   | Collation | Nullable |                  Default
-- -----------+---------+-----------+----------+--------------------------------------------
-- id        | integer |           | not null | nextval('markers_input_id_seq1'::regclass)
-- marker_id | text    |           | not null |
-- Indexes:
-- "markers_input_pkey1" PRIMARY KEY, btree (id)
-- "markers_input_marker_id_key1" UNIQUE CONSTRAINT, btree (marker_id)

-- Try adding the second index:
-- drop index markers_input_marker_id_key1;
-- create unique index markers_input_marker_id_key1 on markers_input (marker_id);
  -- ERROR:  could not create unique index "markers_input_marker_id_key1"
  --DETAIL:  Key (marker_id)=(chr.11_72065374) is duplicated.

-- Try adding the UNIQUE CONSTRAINT
-- alter table markers_input add constraint unique_markerid unique using index markers_input_marker_id_key1;
-- This fails because the query list has some duplicates:
  -- gobii_dev=# copy public.markers_input (marker_id)
  -- gobii_dev-# FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.10k';
  -- ERROR:  duplicate key value violates unique constraint "unique_markerid"
  -- DETAIL:  Key (marker_id)=(chr.3_22444223) already exists.
  -- CONTEXT:  COPY markers_input, line 4123


-- ---- 1000 markers ----

-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.1000';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- -- Time: 2527.952 ms (00:02.528)
-- -- Time: 2291.129 ms (00:02.291)
-- -- Time: 2287.571 ms (00:02.288)


-- ---- 10,000 markers ----

-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.10k';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- Time: 505186.891 ms (08:25.187)
-- Time: 22841.853 ms (00:22.842)
-- Time: 22746.955 ms (00:22.747)


-- ---- 100,000 markers ----

-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.100k';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- -- Time: 2610467.846 ms (43:30.468)
-- -- Time: 252815.186 ms (04:12.815)
-- -- Time: 240088.725 ms (04:00.089)


---- 1,000,000 markers ----
\echo 1,000,000 markers, SNPs:
-- Load the list of markers to query.
truncate markers_input;
copy public.markers_input (marker_id)
FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.1m';

-- Three reps
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

-- ERROR:  could not resize shared memory segment "/PostgreSQL.470109345" to 8388608 bytes: No space left on device

-- COPY 1008373
-- Time: 428104.122 ms (07:08.104)
-- COPY 1008373
-- Time: 431697.211 ms (07:11.697)
-- COPY 1008373
-- Time: 428367.252 ms (07:08.367)
-- ?? Why is the query finding those extra 8373 rows?


---- 5,000,000 markers ----
\echo 5,000,000 markers, SNPs:
-- Load the list of markers to query.
truncate markers_input;
copy public.markers_input (marker_id)
FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.5m';

-- Three reps
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

-- ERROR:  could not resize shared memory segment "/PostgreSQL.1506999503" to 67244032 bytes: No space left on device

-- COPY 6049936
-- Time: 3090762.864 ms (51:30.763)
-- COPY 6049936
-- Time: 3085781.106 ms (51:25.781)
-- COPY 6049936
-- Time: 3082737.023 ms (51:22.737)


-- ---- explain analyze:

-- explain analyze select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id


-- ---- 10,000 markers, "inner join" ----

-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/polyploid_indel_benchmarking/randomlists/pg_markers.10k';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs
-- inner join markers_input mi on mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs
-- inner join markers_input mi on mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_snp mgs
-- inner join markers_input mi on mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- -- Time: 23229.023 ms (00:23.229)
-- -- Time: 22808.520 ms (00:22.809)
-- -- Time: 22905.517 ms (00:22.906)
