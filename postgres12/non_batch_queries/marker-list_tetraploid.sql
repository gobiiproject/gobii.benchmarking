-- All samples for a list of markers:
-- Tetraploids

-- Create the table to hold the queried marker names.
-- Don't make marker_id column UNIQUE, since the random markers might include a few duplications.
--
-- CREATE TABLE public.markers_input
-- (
--     id SERIAL PRIMARY KEY,
--     marker_id text 
-- )
-- WITH (
--     OIDS = FALSE
-- )
-- TABLESPACE pg_default;


---- 1000 markers ----
-- \echo 1000 markers
-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/gobii.benchmarking/postgres12/random_lists/Tetraploid/pg_markers_tetraploid.1000';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1000Samples.csv' with CSV DELIMITER E'\t';

-- -- Time: 2834.129 ms (00:02.834)
-- -- Time: 2608.600 ms (00:02.609)
-- -- Time: 2599.160 ms (00:02.599)


-- ---- 10,000 markers ----
-- \echo 10,000 markers
-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/gobii.benchmarking/postgres12/random_lists/Tetraploid/pg_markers_tetraploid.10k';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_10kSamples.csv' with CSV DELIMITER E'\t';

-- -- Time: 24764.006 ms (00:24.764)
-- -- Time: 24728.124 ms (00:24.728)
-- -- Time: 24598.360 ms (00:24.598)


-- ---- 100,000 markers ----
-- \echo 100,000 markers
-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/gobii.benchmarking/postgres12/random_lists/Tetraploid/pg_markers_tetraploid.100k';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_100kSamples.csv' with CSV DELIMITER E'\t';

-- -- Time: 242980.987 ms (04:02.981)
-- -- Time: 241643.690 ms (04:01.644)
-- -- Time: 241205.179 ms (04:01.205)
-- --
-- -- Time: 2202381.156 ms (36:42.381)
-- -- Time: 247512.323 ms (04:07.512)
-- -- Time: 254444.832 ms (04:14.445)


-- ---- 1,000,000 markers ----
-- \echo 1,000,000 markers
-- -- Load the list of markers to query.
-- truncate markers_input;
-- copy public.markers_input (marker_id)
-- FROM '/data/gobii.benchmarking/postgres12/random_lists/Tetraploid/pg_markers_tetraploid.1m';

-- -- Three reps
-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

-- copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id)
-- to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_1mSamples.csv' with CSV DELIMITER E'\t';

-- -- ERROR:  could not resize shared memory segment "/PostgreSQL.1084951465" to 8388608 bytes: No space left on device
-- --
-- -- Time: 823818.451 ms (13:43.818)
-- -- Time: 511361.486 ms (08:31.361)
-- -- Time: 513584.595 ms (08:33.585)


-- ---- 5,000,000 markers ----
\echo 5,000,000 markers
-- Load the list of markers to query.
truncate markers_input;
copy public.markers_input (marker_id)
FROM '/data/gobii.benchmarking/postgres12/random_lists/Tetraploid/pg_markers_tetraploid.5m';

-- Three reps
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_tetraploid mgs, markers_input mi
where mgs.marker_name = mi.marker_id)
to '/data/polyploid_indel_benchmarking/pg12_output/tetraploid_dataset/allMarkers_5mSamples.csv' with CSV DELIMITER E'\t';

-- ERROR:  could not resize shared memory segment "/PostgreSQL.1506999503" to 67244032 bytes: No space left on device
--
-- COPY 5041946
-- Time: 3380816.193 ms (56:20.816)
-- COPY 5041946
-- Time: 3391332.433 ms (56:31.332)
-- COPY 5041946
-- Time: 3373920.178 ms (56:13.920)


-- ---- explain analyze:

-- explain analyze select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
-- from marker_genotype_tetraploid mgs, markers_input mi
-- where mgs.marker_name = mi.marker_id

