-- All markers for a list of samples:

drop table samples_input;
CREATE TABLE public.samples__input
(
    id SERIAL PRIMARY KEY,
    sample_id text UNIQUE NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


-- PROGRAM transposes the sample list from one column to a single text field with tab punctuation in it.
-- Default FORMAT of input file is text, so '\t' would be interpreted as a DELIMITER which we don't want so we change the DELIMITER.
truncate samples_input;
copy public.samples_input (sample_id)
FROM PROGRAM 'cat /data/polyploid_indel_benchmarking/randomlists/pg_samples.100 | tr "\n" "\t"'
DELIMITER E',';


DROP FUNCTION IF EXISTS parse_sample_list(jsonb, text);
CREATE OR REPLACE FUNCTION parse_sample_list(genotype jsonb, sample_ids text)
RETURNS text
STABLE
PARALLEL SAFE
AS $$
import json, collections
obj = json.loads(genotype, object_pairs_hook=collections.OrderedDict)
sids = sample_ids.split()
parsed = []
for x in range (len(sids)):
    parsed.append(obj[sids[x]])
return '\t'.join(parsed)
$$ LANGUAGE plpython3u;

-- Test whether OrderedDict is unnecessary and slowing us down.
DROP FUNCTION IF EXISTS parse_sample_list(jsonb, text);
CREATE OR REPLACE FUNCTION parse_sample_list(genotype jsonb, sample_ids text)
RETURNS text
STABLE
PARALLEL SAFE
AS $$
import json
obj = json.loads(genotype)
sids = sample_ids.split()
parsed = []
for x in range (len(sids)):
    parsed.append(obj[sids[x]])
return '\t'.join(parsed)
$$ LANGUAGE plpython3u;
-- Works. Twenty minutes faster.

DROP FUNCTION IF EXISTS parse_sample_list(jsonb, text);
CREATE OR REPLACE FUNCTION parse_sample_list(genotype jsonb, sample_ids text)
RETURNS text
STABLE
PARALLEL SAFE
TRANSFORM FOR TYPE jsonb
AS $$
sids = sample_ids.split()
parsed = []
for x in range (len(sids)):
    parsed.append(genotype[sids[x]])
return '\t'.join(parsed)
$$ LANGUAGE plpython3u;
-- Works! ("TRANSFORM FOR TYPE jsonb" is magic.)
-- 35 minutes faster, but rows are out of order relative to marker_genotype_snp.marker_name.


copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_100Samples.csv' with CSV DELIMITER E'\t';



