#1000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
inner join markers_input_1k mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/1kMarkers_allSamples_nonctg.csv' with CSV DELIMITER E'\t';

copy (select mgs.marker_name, parse_all_samples(genotype)
from marker_genotype_snp mgs
inner join markers_input_1k mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/1kMarkers_allSamples_nonctg_b.csv' with CSV DELIMITER E'\t';

#10000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
inner join markers_input mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/10kMarkers_allSamples_nonctg.csv' with CSV DELIMITER E'\t';

#100000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
inner join markers_input mi on mgs.marker_name=mi.marker_id) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/100kMarkers_allSamples_nonctg.csv' with CSV DELIMITER E'\t';

##########################################################################################
#1000 markers ctg
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 1100) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/1kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#10,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 10100) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/10kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#100,000 markers
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 100100) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/100kMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#1,000,000 markers
--NOTE: Moved to batch queries
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 1000100) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/1mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

#5,000,000 markers
--2084034.623 ms (34:44.035) --NOTE: Moved to batch queries
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 3000, 'variant.')
from marker_genotype_snp mgs
where marker_genotype_id < 5000100) to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/5mMarkers_allSamples.csv' with CSV DELIMITER E'\t';

##########################################################################################
#All markers for N samples, non-contiguous.  Non-batch (six cpus).
#100 samples 
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id, 100)
from marker_genotype_snp mgs, samples_input si)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_100Samples.csv' with CSV DELIMITER E'\t';
--      COPY 20000000
--      Time: 8058279.680 ms (02:14:18.280)
--      Time: 8072590.297 ms (02:14:32.590)
--	Time: 8043182.116 ms (02:14:03.182)

#50 samples
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id, 50)
from marker_genotype_snp mgs, samples_input si)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_50Samples.csv' with CSV DELIMITER E'\t';
-- Time: 8012633.366 ms (02:13:32.633)
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_50Samples.csv' with CSV DELIMITER E'\t';
-- Time: 8007795.688 ms (02:13:27.796)

#2000 samples
truncate samples_input;
copy public.samples_input (sample_id)
FROM PROGRAM 'cat /data/polyploid_indel_benchmarking/randomlists/pg_samples.2000 | tr "\n" "\t"'
DELIMITER E',';
copy (select mgs.marker_name, parse_sample_list(mgs.genotype, si.sample_id)
from marker_genotype_snp mgs, samples_input si)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/allMarkers_2kSamples.csv' with CSV DELIMITER E'\t';
-- Time: 10178010.734 ms (02:49:38.011)
-- Time: 10209038.140 ms (02:50:09.038)

######################################################
#All markers for N samples, contiguous.  Non-batch (six cpus).
#100 samples
copy (select mgs.marker_name, parse_sample_range(genotype, 1, 100, 'variant.')
from marker_genotype_snp mgs)
to '/data/polyploid_indel_benchmarking/pg12_output/snp_dataset/AllMarkers_100Samples_ctg.csv' with CSV DELIMITER E'\t';

