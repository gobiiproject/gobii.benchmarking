#!/bin/sh

# Values for <number>: 1k, 5k, 10k, 50k, 100k, 500k, 1m, 5m

# Indels
./selectmarkers Indels/markernames.indel 1000 Indels/pg_markers_indel.1000 discardable
./selectmarkers Indels/markernames.indel 5000 Indels/pg_markers_indel.5000 discardable
./selectmarkers Indels/markernames.indel 10000 Indels/pg_markers_indel.10k discardable
./selectmarkers Indels/markernames.indel 50000 Indels/pg_markers_indel.50k discardable
./selectmarkers Indels/markernames.indel 100000 Indels/pg_markers_indel.100k discardable
./selectmarkers Indels/markernames.indel 500000 Indels/pg_markers_indel.500k discardable
./selectmarkers Indels/markernames.indel 1000000 Indels/pg_markers_indel.1m discardable
./selectmarkers Indels/markernames.indel 5000000 Indels/pg_markers_indel.5m discardable

# Tetraploid
./selectmarkers Tetraploid/markernames.tetraploid 1000 Tetraploid/pg_markers_tetraploid.1000 discardable
./selectmarkers Tetraploid/markernames.tetraploid 5000 Tetraploid/pg_markers_tetraploid.5000 discardable
./selectmarkers Tetraploid/markernames.tetraploid 10000 Tetraploid/pg_markers_tetraploid.10k discardable
./selectmarkers Tetraploid/markernames.tetraploid 50000 Tetraploid/pg_markers_tetraploid.50k discardable
./selectmarkers Tetraploid/markernames.tetraploid 100000 Tetraploid/pg_markers_tetraploid.100k discardable
./selectmarkers Tetraploid/markernames.tetraploid 500000 Tetraploid/pg_markers_tetraploid.500k discardable
./selectmarkers Tetraploid/markernames.tetraploid 1000000 Tetraploid/pg_markers_tetraploid.1m discardable
./selectmarkers Tetraploid/markernames.tetraploid 5000000 Tetraploid/pg_markers_tetraploid.5m discardable

