#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main (int argc, char *argv[]) {

  if (argc < 5) {
    printf("Usage: %s <listfile> <number> <pg file> <hdf5 file>\n", argv[0]);
    printf("E.g. %s markernames.all 1000 pg_markers.1000 h5_markers.1000\n", argv[0]);
    printf("Generate a list of <number> valid marker names, selected at random.\n");
    printf("The listfile contains the names of all the markers, separated by newline.\n");
    printf("Values for <number>: 1k, 5k, 10k, 50k, 100k, 500k, 1m, 5m\n");
    return 0;
  }
  /* Read the arguments. */
  char *listfilename = argv[1];
  int outcount = atoi(argv[2]);
  char *postgresfile = argv[3];
  char *hdf5file = argv[4];

  // There are exactly 20M markernames in listfile. All <20 characters.
  char **markers = malloc(20000000 * sizeof(char *));
  for (int j = 0; j < 20000000; j++) 
    markers[j] = malloc(20 * sizeof(char));

  /* Read in the list of marker positions into array markers[]. */
  FILE *infile = fopen(listfilename, "r");
  int i = 0;
  while (fgets(markers[i], 20, infile) != NULL) {
    strtok(markers[i], "\n");
    /* printf("i = %i, markers[i] = %s\n", i, markers[i]); */
    i++;
  }
  fclose(infile);

  // Seed the random number generator.
  time_t starttime = time(NULL);
  srand(starttime);

  // Output a random marker name to the Postgres file, and its position to the HDF5 file.
  FILE *pgfile = fopen(postgresfile, "w");
  FILE *h5file = fopen(hdf5file, "w");
  int index;
  for (i = 0; i < outcount; ++i) {
    index = rand() % 20000000;
    /* printf("index = %i, markers[index] = %s\n", index, markers[index]); */
    fprintf(pgfile, "%s\n", markers[index]);
    fprintf(h5file, "%i\n", index);
  }
  fclose(pgfile);
  fclose(h5file);
  printf("Done\n");
}
