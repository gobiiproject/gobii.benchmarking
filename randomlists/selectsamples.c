#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main (int argc, char *argv[]) {

  if (argc < 4) {
    printf("Usage: %s <number> <pg file> <hdf5 file>\n", argv[0]);
    printf("E.g. %s 1000 pg_samples.1000 h5_samples.1000\n", argv[0]);
    printf("Generate a list of <number> valid sample names, selected at random.\n");
    printf("Values for <number>: 50, 100, 500, 1000, 2000, 3000\n");
    return 0;
  }
  /* Read the arguments. */
  int outcount = atoi(argv[1]);
  char *postgresfile = argv[2];
  char *hdf5file = argv[3];

  // Seed the random number generator.
  time_t starttime = time(NULL);
  srand(starttime);

  // Output a random sample name to the Postgres file, and its position to the HDF5 file.
  FILE *pgfile = fopen(postgresfile, "w");
  FILE *h5file = fopen(hdf5file, "w");
  char *name = malloc(100);
  char *prefix = "variant.";
  int index;
  for (int i = 0; i < outcount; ++i) {
    index = rand() % 3000;
    sprintf(name, "%s%i", prefix, index); 
    fprintf(pgfile, "%s\n", name);
    fprintf(h5file, "%i\n", index);
  }
  fclose(pgfile);
  fclose(h5file);

}
